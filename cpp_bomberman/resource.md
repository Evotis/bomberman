#ECS model

##ecs resources:
  * [le mec qui fait un cours](http://goo.gl/HM0ET)
  * [wiki spécial ECS](http://goo.gl/oKKLFB)
  * [page wiki](http://goo.gl/pBpxwf)
  * [post stack](http://goo.gl/ZgA7gg)
  * [autre cours plus succint](http://goo.gl/IW6Rt)
  * [implementation possible](http://goo.gl/2af0vO)

===================

++ -> many

-- -> not sure for good category

###Entity:
  * ++ bomb (may be many bombs with many effects)
     wall
     box
  * ++ bonus
  *    player
  *    AI
  * ++ bonus
    *    bSpeed
    *    bBomb
    *    ...
  * -- menu

### Component:
  * physic
  * script
  * score
  * inventory
  * explode
  * move
  * respawn
  * die
  * putBomb
  * position
  * texture
  * destroyable
  * life
  * sound

### System:
  * collision
  * input
  * render
  * ...


### ex:
| Component     | bomb   | player | AI    | box    | bSpeed | bBomb  | wall   |
| :----:        | :----: | :---:  | :---: | :----: | :----: | :----: | :----: |
| position      | x      | x      | x     | x      | x      | x      | x      |
| life          | o      | x      | x     | o      | o      | o      | o      |
| physic        | o      | x      | o     | o      | o      | o      | o      |
| script        | o      | o      | x     | o      | o      | o      | o      |
| explode       | x      | o      | o     | o      | o      | o      | o      |
| score         | o      | x      | o     | o      | o      | o      | o      |
| sound         | x      | x      | x     | x      | x      | x      | o      |
| move          | o      | o      | o     | o      | o      | o      | o      |
| respawn       | o      | x      | x     | o      | o      | o      | o      |
| die           | x      | x      | x     | x      | x      | x      | o      |
| putBomb       | o      | x      | x     | o      | o      | o      | o      |
| texture       | x      | x      | x     | x      | x      | x      | x      |
| taken         | o      | o      | o     | o      | x      | x      | o      |
| destroyable   | o      | o      | o     | x      | o      | o      | o      |
| generateBonus | o      | o      | o     | x      | o      | o      | o      |

