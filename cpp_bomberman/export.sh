#! /bin/sh

# allow to set or unset the LB_LIBRARY_PATH env variable

if [ "$1" = "set" ]; then
  export LD_LIBRARY_PATH=`pwd`/LibBomberman_linux_x64/libs/;
  echo "LD_LIBRARY_PATH='$LD_LIBRARY_PATH'"

elif [ "$1" = "unset" ]; then
  export LD_LIBRARY_PATH="";
  echo "LD_LIBRARY_PATH='$LD_LIBRARY_PATH'"

else
  echo "USAGE: source ./export.sh [set|unset]"
fi

