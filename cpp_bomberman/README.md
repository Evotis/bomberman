## to add new remote:

    cd path/to/repo ; git remote add <your remote name> git@bitbucket.org:/n0t/cpp_bomberman

## Install the lib
Makefile and export.sh script are wrote to use the graphic’s library at the root of the repo.
Please make sure the library will not be included in commits.
The `.gitignore`, normally, prevents against its inclusion in the working directory, but not if
you put the `LibBomberman_linux_x64` anywhere else than in the root.

Also, you will not be able to compile anymore unless you change the Makefile and fuck everyone’s precious time.
But you free to go ;)

## to use the script
This script allows you to set or unset `LD_LIBRARY_PATH` variable in an other way that by hand.
Like this variable is an environment’s one, you’ll need to [source](http://goo.gl/46c3JD) the script to make changes persistants.

1. set

        . ./export.sh set

2. unset

        . ./export.sh unset

## Keycode:

    SDLK_UP : Monter
    SDLK_DOWN : Descendre
    SDLK_LEFT : Gauche
    SDLK_RIGHT : Droite
    SDLK_ESPACE : Poser une bombe

    Key en zqsd