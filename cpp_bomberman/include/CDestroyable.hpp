/*
** CDestroyable.hh for include in /home/mohame_e/project/cpp/cpp_bomberman/include
**
** Made by Ahmed Mohamed Ali
** Login   <mohame_e@epitech.net>
**
** Started on  Wed May 14 15:09:05 2014 Ahmed Mohamed Ali
** Last update Wed May 14 15:09:05 2014 Ahmed Mohamed Ali
*/

#ifndef CDESTROYABLE_HPP_
# define CDESTROYABLE_HPP_

#include "AComponent.hpp"

namespace Component {
  class Destroyable :public AComponent {
    public:
      // ctors & dtors
      Destroyable();
      Destroyable(const Destroyable& );
      Destroyable& operator=(const Destroyable& );
      ~Destroyable();

    public:
  };
}

#endif  /* !CDESTROYABLE_HPP_ */
