/*
** CScript.hh for include in /home/mohame_e/project/cpp/cpp_bomberman/include
**
** Made by Ahmed Mohamed Ali
** Login   <mohame_e@epitech.net>
**
** Started on  Wed May 14 15:09:05 2014 Ahmed Mohamed Ali
// Last update Fri May 23 14:46:48 2014 nelson touron
*/

#ifndef CSCRIPT_HPP_
# define CSCRIPT_HPP_

#include "AComponent.hpp"

namespace Component {
  class Script :public AComponent {
  public:
    // ctors & dtors
    Script();
    Script(const Script&);
    Script& operator=(const Script&);
    ~Script();
  };
}

#endif  /* !CSCRIPT_HPP_ */
