//
// SRender.hpp for bomberman in /home/eyzat_f/projets/cpp/bomberman/cpp_bomberman
//
// Made by florian eyzat
// Login   <eyzat_f@epitech.net>
//

#ifndef SRENDER_HPP_
# define SRENDER_HPP_

#include <Game.hh>
#include <SdlContext.hh>
#include <Model.hh>
#include <vector>
#include "RPlate.hpp"
#include "RSkyBox.hpp"
#include "World.hpp"
#include "RScreen.hpp"

#include "AComponent.hpp"

#include <OpenGL.hh>
#include <string>
#include <glm/glm.hpp>

class SRender : public gdl::Game
{
private:
  int X;
  int Y;
  gdl::SdlContext	  _context;
  gdl::Clock		    _clock;
  gdl::Input		    _input;
  gdl::BasicShader	_shader;
  glm::mat4         projection;
  int	sX;
  int	sY;
  std::vector<entity *> _player;
  std::map<eType, AObject *> allstar;
  glm::vec3         cam;
  glm::vec3         lok;
  
  bool	multi;
  int	nbPlayer;
  Screen  *_pause;
  SkyBox  *sky;
  bool    NOGROUND;

  int	eSpeed;
  bool initialize();
  bool initialize(gdl::SdlContext & context);
  bool update();
  void updateCam(float, float);
  void draw(){}
  void mydraw(entity *);
  bool makeCont(int, int);
  bool makeBord(int, int, int);
  bool makeMarvin();

  bool fillMap();
  void cubeEntity(entity *);
  void drawEntity(entity *);

  void create_geometry(gdl::Geometry);
  void updateEntity(entity *obj);
  void camEntity(entity *obj);
  void useWindow(entity *obj, unsigned int player_num, unsigned int max_player);
  void actionEntity(entity *obj, int x, int z, int rotation);
  void saveScore();
  bool updateP();
  void playing();
  void in_pause();

public:
  SRender(int, int);
  // SRender(int, int, int, gdl::SdlContext & context);
  SRender(int, int, gdl::SdlContext & context, std::string);
  virtual ~SRender();

  bool start();

};

#endif // !SRENDER_HPP_
