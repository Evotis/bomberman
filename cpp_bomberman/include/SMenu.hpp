//
// Menu.hpp for bomberman in /home/eyzat_f/projets/cpp/bomberman/cpp_bomberman
//
// Made by florian eyzat
// Login   <eyzat_f@epitech.net>
//

#ifndef SMENU_HPP_
# define SMENU_HPP_

#include <Game.hh>
#include <Game.hh>
#include <SdlContext.hh>
#include <Model.hh>
#include <vector>
#include "RCube.hpp"
#include "RPlate.hpp"
#include "RScreen.hpp"
#include "RButton.hpp"

#include <OpenGL.hh>
#include <string>
#include <glm/glm.hpp>

#include <sstream>
#include <iostream>
#include <ctime>
#include <unistd.h>

#include "Generator.hpp"
#include "Systems.hpp"
#include "CPosition.hpp"
#include "AComponent.hpp"
#include "World.hpp"
#include "Save.hpp"
#include "SRender.hpp"
#include <sstream>
#include <string>
#include <fstream>

class Menu : public gdl::Game
{
private:
  int X;
  int Y;
  gdl::SdlContext	  _context;
  gdl::Clock		    _clock;
  gdl::Input		    _input;
  gdl::BasicShader	_shader;
  glm::mat4         projection;
  int	sX;
  int	sY;


  std::map<int, std::string> _texturepack;
  std::map<ePage, Screen*> _screens;
  std::map<int, Screen*> _numbers;

public:
  Menu(int, int);
  virtual ~Menu();

  bool initialize();
  bool update();
  void updateCam(float, float);
  void draw();
  //
  bool start();
  bool addPict(std::string path, ePage, bool);
  void initializeMenu();
  bool nb();
  void draw_numbers();
  bool launch_game(int i);
  void printValue(int value, glm::vec3 pos);
  void limit(eVal t, int min, int max);
  void draw_score();
};

#endif
