/*
** CScreen.hh for include in /home/mohame_e/project/cpp/cpp_bomberman/include
**
** Made by Ahmed Mohamed Ali
** Login   <mohame_e@epitech.net>
**
** Started on  Wed May 14 15:09:05 2014 Ahmed Mohamed Ali
** Last update Wed May 14 15:09:05 2014 Ahmed Mohamed Ali
*/

#ifndef CSCREEN_HPP_
# define CSCREEN_HPP_

#include "AComponent.hpp"

namespace Component {
  class Screen :public AComponent {
  	int			posx;
  	int			posy;
  	int			width;
  	int			height;

    public:
      // ctors & dtors
      Screen();
      Screen(const Screen&);
      Screen& operator=(const Screen&);
      ~Screen();
  	 
  	 void	window();
  	 void	setScreen(glm::vec4 &);
  };
}

#endif  /* !CSCREEN_HPP_ */
