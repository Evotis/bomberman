//
// AComponent.hh for include in /home/n0t/dev/cpp/bomberman/cpp_bomberman/include
//
// Made by thomas nieto
// Login   <nieto_t@epitech.net>
//
// Started on  Sat May 10 13:34:55 2014 thomas nieto
// Last update Sat May 10 13:34:55 2014 thomas nieto
//

#ifndef ACOMPONENT_HPP_
# define ACOMPONENT_HPP_

#include "Bomber.hpp"
#include <Game.hh>
#include <Clock.hh>
#include <Input.hh>
#include <SdlContext.hh>
#include <Geometry.hh>
#include <Texture.hh>
#include <BasicShader.hh>
#include <Model.hh>
#include <OpenGL.hh>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace Component {

  enum type_t {
    POSITION,
    LIFE,
    PHYSIC,
    SCRIPT,
    EXPLODE,
    MOVE,
    RESPAWN,
    DIE,
    PUTBOMB,
    TAKEN,
    DESTROYABLE,
    GENERATEBONUS,
    INVENTORY,
    SCORE,
    SOUND,
    TYPE,
    ALIVE,
    GEOMETRY,
    SCREEN,
    KEY,
    ISTAKEN
  };

  class AComponent {
    private:
      // ctors & dtors
      AComponent(const AComponent&);
      AComponent& operator=(const AComponent&);

    protected:
      type_t type_;

      //ctors && dtors
      AComponent();

    public:
      // ctors & dtors
      virtual ~AComponent();

      // getters
      type_t getType() const;

      // setters

      // actions

  };
}

#endif /* !ACOMPONENT_HPP_ */
