#ifndef SCOLISION_HPP_
#define SCOLISION_HPP_

#include <map>
#include "Systems.hpp"
#include "World.hpp"

class Collision
{
public:
	Collision();
	~Collision();
	Collision(Collision const &);
	Collision 	&operator=(Collision const &);

	bool 			onBonus(entity *, entity *);
	bool 			dieRight(entity *, entity *);
	bool 			dieLeft(entity *, entity *);
	bool			goAhead(entity *, entity *);
	bool			doNothing(entity *, entity *);
	bool			isColliding(t_pos, t_pos, entity *);
	bool			killAndGo(entity *ent, entity*);

private:
	std::map<eType, std::map<eType, bool (Collision::*)(entity *, entity *)> >  instr;

};

extern Collision g_collision;

#endif