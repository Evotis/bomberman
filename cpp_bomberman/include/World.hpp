//
// World.hpp for bomber in /home/touron_n/eph/qtree
//
// Made by nelson touron
// Login   <touron_n@epitech.net>
//
// Started on  Mon May 26 18:32:31 2014 nelson touron
// Last update Sun Jun 15 16:51:04 2014 nelson touron
//

#ifndef _QUAD_HPP_
#define _QUAD_HPP_

#include <list>
#include <iostream>
#include <map>
#include "Entity.hpp"

typedef std::map<Component::type_t, Component::AComponent *> entity;

const int       nbChild = 4;
const int       entPerQuad = 5;

typedef struct                          s_WorldSorter {
  int                                   xMin;
  int                                   xMax;
  int                                   yMin;
  int                                   yMax;
}                                       t_WorldSorter;

class					World {

private:
  std::list<entity*>			entList_;
  size_t				lnt_;
  size_t				wdt_;
  t_pos					xMark_;
  t_pos					yMark_;
  int					dpt_;

  World					*childs_[nbChild];
  t_WorldSorter                         sorter_[nbChild];

  World(World const &);
  World					&operator=(World const &);
  void					initSorter();

public:
  // ctors & dtors
  World(const std::list<entity *>&, int const, int const, int const, int const , int const);
  World(int const, int const);
  ~World();

  // actions
  void					split();
  void					push(entity * const);
  void					operator()(t_pos const, t_pos const, t_entlist &) const;
  entity				*pop(entity * const);
  void					dump(std::ostream &) const;
  void					deleteTree();
  // getters
  int					getxMark() const;
  int					getyMark() const;
  size_t				getX() const;
  size_t				getY() const;
  int					getdpt() const;
  const std::list<entity *>		&getEntList() const;
  World					*getChild(int) const;
  // setters
  void					setX(size_t);
  void					setY(size_t);
};

// Overload of "<<" for dumping tree
std::ostream& operator<<(std::ostream&, World&);

extern World *g_world;

#endif
