//
// Bomber.hh for include in /home/n0t/dev/cpp/bomberman/cpp_bomberman/include
//
// Made by thomas nieto
// Login   <nieto_t@epitech.net>
//
// Started on  Sat May 10 13:37:56 2014 thomas nieto
// Last update Sat May 10 13:37:56 2014 thomas nieto
//

#ifndef BOMBER_HH_
# define BOMBER_HH_

#include <iostream>

// every typedef
typedef size_t t_pos;
typedef int t_id;
typedef int t_life;

int const none = -1;

# endif /* !BOMBER_HH_ */
