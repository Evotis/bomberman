#include <exception>
using namespace std;
 
class SaveError: public exception
{
public:
    SaveError(string const& phrase="") throw()
         :m_phrase(phrase)
    {}
 
     virtual const char* what() const throw()
     {
         return m_phrase.c_str();
     }

    virtual ~SaveError() throw()
    {}
 
private:
    string m_phrase;
};