#ifndef ALIVE_HPP_
#define ALIVE_HPP_

#include "AComponent.hpp"

namespace Component
{
	class Alive : public Component::AComponent
	{
	public:
		Alive(bool = true);
		~Alive();
		Alive(Alive const &);
		Alive 	&operator=(Alive const &);

		bool	isAlive() const;
		void	setDeath();
		void	setRebirth();

	private:
		bool	a;
	};
}

#endif
