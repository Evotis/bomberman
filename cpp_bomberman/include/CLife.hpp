#ifndef	CLIFE_HH_
#define CLIFE_HH_

#include "Bomber.hpp"
#include "AComponent.hpp"

namespace Component
{
	class Life : public AComponent
	{
	public:
		Life();
		~Life();
		Life(Life const &oth);
		Life 		&operator=(Life const &oth);

		void		loseLife();
		void		setLife(int nLife);
		t_life		getLife() const;
		t_life		getMaxLife() const;
	private:
		t_life		max;
		t_life 		current;
	};
};

#endif