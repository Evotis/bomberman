/*
** CDie.hh for include in /home/mohame_e/project/cpp/cpp_bomberman/include
**
** Made by Ahmed Mohamed Ali
** Login   <mohame_e@epitech.net>
**
** Started on  Wed May 14 15:09:05 2014 Ahmed Mohamed Ali
** Last update Wed May 14 15:09:05 2014 Ahmed Mohamed Ali
*/

#ifndef CDIE_HPP_
# define CDIE_HPP_

#include "AComponent.hpp"

namespace Component {
  class Die :public AComponent {
    public:
      // ctors & dtors
      Die();
      Die(const Die&);
      Die& operator=(const Die&);
      ~Die();
  };
}

#endif  /* !CDIE_HPP_ */
