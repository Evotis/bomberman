#ifndef CINVENTORY_HH_
#define CINVENTORY_HH_

#include "AComponent.hpp"

namespace Component {
	class Inventory : public AComponent
	{
	public:
		Inventory(int range = 2, int speed = 100, int nbBomb = 1);
		~Inventory();
		Inventory(Inventory const &);
		Inventory 	&operator=(Inventory const &);

		int			getRange() const;
		int			getSpeed() const;
		int			getNbBomb() const;
		int			getMaxBomb() const;

		void		addRange();
		void		addSpeed();
		void		addNbBomb();
		void		setRange(int);
		void		decSpeed();
		void		setSpeed(int);
		void		setNbBomb(int);
		void		subNbBomb();
		void		addTime(float);
		void		reset();
		bool 		canMove();

	private:
		int 	speed;
		int		nbBomb;
		int		maxBomb;
		int		range;
		int 	time_since_last_move;
	};
};
#endif
