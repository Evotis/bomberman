/*
** CPosition.hpp for include in /home/mohame_e/project/cpp/cpp_bomberman/include
**
** Made by Ahmed Mohamed Ali
** Login   <mohame_e@epitech.net>
**
** Started on  Wed May 14 15:09:05 2014 Ahmed Mohamed Ali
// Last update Fri May 23 14:47:58 2014 nelson touron
*/

#ifndef CPOSITION_HPP_
# define CPOSITION_HPP_

#include "AComponent.hpp"

namespace Component {
  class Position :public AComponent {
  private:
    t_pos x_;
    t_pos y_;
    t_pos s_x;
    t_pos s_y;

  public:
    Position(int x, int y);
    Position(const Position&);
    Position &operator=(const Position&);
    ~Position();

  public:
    t_pos  getX() const;
    t_pos  getY() const;
    t_pos  getStartX() const;
    t_pos  getStartY() const;
    void   SetX(t_pos x);
    void   SetY(t_pos y);
    void   setPos(int, int);
  };
}

#endif  /* !CPOSITION_HPP_ */
