//
// Generator.hpp for include in /home/n0t/dev/cpp/bomberman/cpp_bomberman/include
//
// Made by thomas nieto
// Login   <nieto_t@epitech.net>
//
// Started on  Tue Jun 10 20:20:10 2014 thomas nieto
// Last update Tue Jun 10 20:20:10 2014 thomas nieto
//

#ifndef GENERATOR_HPP_
# define GENERATOR_HPP_

#include <vector>
#include <utility>
#include <cstdlib>
#include <time.h>

#include "Entity.hpp"
#include "GeneratorException.hpp"

const t_pos empty = -1;

typedef float density;
typedef std::pair<t_pos, t_pos> t_defend;
typedef std::vector<std::pair<t_pos, t_pos> > t_savePos;

class Generator {

  private:
    // coplian's form
    Generator(const Generator&);
    Generator& operator=(const Generator&);

    // attributes
    t_pos x_;
    t_pos y_;
    t_defend begin_;
    t_defend end_;
    t_pos surface_;
    density box_;
    t_pos ia_;
    density bonus_;
    t_savePos pushed_;
    std::list<entity *> init_;

    // actions
    void placeWall();
    void placeBox();
    void placeIa();
    bool pushed(const std::pair<t_pos, t_pos>&, t_pos) const;
    void createBonus(char, const std::pair<t_pos, t_pos>&);
    t_pos checkProc(density) const;

  public:
    // ctors & dtors
    Generator(t_pos = 0, t_pos = 0, density = 0.25, density = 0.25, density = 0.25);
    ~Generator();

    // getters
    const std::list<entity *>& get() const;
    t_pos getX() const;
    t_pos getY() const;
    const t_defend& getBegin() const;
    const t_defend& getEnd() const;
    t_pos getSurface() const;
    density getBox() const;
    density getIa() const;
    density getBonus() const;
};

std::ostream& operator<<(std::ostream&, const Generator&);

# endif // !GENERATOR_HPP_

