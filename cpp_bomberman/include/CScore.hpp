/*
** CScore.hh for include in /home/mohame_e/project/cpp/cpp_bomberman/include
**
** Made by Ahmed Mohamed Ali
** Login   <mohame_e@epitech.net>
**
** Started on  Wed May 14 15:09:05 2014 Ahmed Mohamed Ali
// Last update Fri May 23 14:47:21 2014 nelson touron
*/

#ifndef CSCORE_HPP_
# define CSCORE_HPP_

#include "AComponent.hpp"

namespace Component {
  class Score :public AComponent {
  public:
    // ctors & dtors
    Score();
    Score(const Score&);
    Score& operator=(const Score&);
    ~Score();

    void	setScore(int nScore);
    void	addScore();
    int		getScore() const;
   	private:
   		int 	score;
  };
}

#endif  /* !CSCORE_HPP_ */
