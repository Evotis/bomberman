#ifndef AOBJECT_HPP_
# define AOBJECT_HPP_

#include <Geometry.hh>
#include <Texture.hh>
#include <SdlContext.hh>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <BasicShader.hh>
#include <iostream>

class AObject
{
protected:
  glm::vec3	_position;
  glm::vec3	_rotation;
  glm::vec3	_scale;

public:
  AObject(int, int, int, float, float, float);

  virtual ~AObject();
  virtual bool initialize(float speed)
  {
    (void)speed;
    return (true);
  }
  virtual void	update(gdl::Clock const &clock, gdl::Input&input);
  virtual void  draw(gdl::AShader &shader, gdl::Clock const &clock) = 0;
  virtual void  setKey(int, int, int, int){}
  virtual void  setWin(int, int, int, int){}
  virtual void  stop(bool){}
  virtual int   setScreen(std::string){return (0);}
  virtual int   setScreen2(std::string){return (0);}
  virtual void  choose_text(){}
  void	translate(glm::vec3 const &v);
  void	rotate(glm::vec3 const & axis, float angle);
  void	scale(glm::vec3 const &scale);
  void  setYRotation(int);
  void	setPosition(int, int, int, int);
  glm::vec3	getPosition();
  glm::mat4	getTransformation();

};

#endif
