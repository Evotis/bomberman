#ifndef CISTAKEN_HPP_
#define CISTAKEN_HPP_

#include "AComponent.hpp"

namespace Component {
	class IsTaken : public Component::AComponent
	{
	public:
		IsTaken();
		~IsTaken();
		IsTaken(IsTaken const &);
		IsTaken	&operator=(IsTaken const &);

		void 	takeIt();
		bool	isItTaken() const;
	private:
		bool	taken;
	};
};

#endif