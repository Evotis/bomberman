/*
** CGeometry.hpp for we in /home/cristi_a/Projects/cave/cpp_bomberman/include
**
** Made by toma cristini
** Login   <cristi_a@epitech.net>
**
** Started on  Wed May  28 15:21:06 2014 toma cristini
** Last update Wed May  28 15:21:06 2014 toma cristini
*/

#ifndef CGeometry_HPP_
# define CGeometry_HPP_

#include "AComponent.hpp"

#include "AObject.hpp"

#include "CType.hpp"
#include "RCube.hpp"
#include "RModel.hpp"

namespace Component {
  class Geometry :public AComponent {
    std::string   texturePath;
    AObject       *cube;
  public:
    // ctors & dtors
    Geometry(int x, int y, eType type);
    Geometry(const Geometry&);
    Geometry& operator=(const Geometry&);
    ~Geometry();

    void            pos(glm::vec3 &, int);
    void            draw(gdl::AShader &shader, gdl::Clock const &clock);
    void            update(gdl::Clock const &clock, gdl::Input &input);
    void            stop(bool);
  };
}

#endif  /* !CGeometry_HPP_ */
