/*
** CSound.hh for include in /home/mohame_e/project/cpp/cpp_bomberman/include
**
** Made by Ahmed Mohamed Ali
** Login   <mohame_e@epitech.net>
**
** Started on  Wed May 14 15:09:05 2014 Ahmed Mohamed Ali
// Last update Fri May 23 14:45:22 2014 nelson touron
*/

#ifndef CSound_HPP_
# define CSound_HPP_

#include "AComponent.hpp"

namespace Component {
  class Sound :public AComponent {
  public:
    // ctors & dtors
    Sound();
    Sound(const Sound&);
    Sound& operator=(const Sound&);
    ~Sound();
  };
}

#endif  /* !CSound_HPP_ */
