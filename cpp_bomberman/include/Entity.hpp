//
// Entity.hh for include in /home/n0t/dev/cpp/bomberman/cpp_bomberman/include
//
// Made by thomas nieto
// Login   <nieto_t@epitech.net>
//
// Started on  Sat May 10 13:34:55 2014 thomas nieto
// Last update Wed Jun 11 19:42:27 2014 nelson touron
//

#ifndef ENTITY_HPP_
# define ENTITY_HPP_

#include <map>
#include <vector>
#include <algorithm>

#include "Bomber.hpp"
#include "AComponent.hpp"
#include "creates.hpp"

// container are entitys
typedef std::map<Component::type_t, Component::AComponent *> entity;
typedef std::vector<entity *> t_entlist;
typedef std::vector<entity *>::iterator t_entlistIt;

void deleteEntity(entity *);

entity *createBomb(t_pos x, t_pos y);
entity *createBox(t_pos x, t_pos y);
entity *createSpeed(t_pos x, t_pos y);
entity *createRange(t_pos x, t_pos y);
entity *createNbBomb(t_pos x, t_pos y);
entity *createIa(t_pos x, t_pos y);
entity *createPlayer(t_pos x, t_pos y);
entity *createWall(t_pos x, t_pos y);
entity *createDeflag(t_pos x, t_pos y);

#endif /* !ENTITY_HPP_ */
