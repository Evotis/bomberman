#pragma once

#include "AObject.hpp"
#include <map>


enum ePage 
{
  OPENING,
  MENU,
  SINGLE,
  MULTI,
  OPTION,
  CREDITS,
  LOAD,
  EDITOR,
  LOADING,
  HIGHSCORE,
  THEMES
};

enum eVal
{
  PAGE,
  WIDTH,
  HEIGHT,
  INVENTORY,
  NB_IA,
  BONUS,
  NB_BOX,
  START,
  PLAYING,
  NO_GROUND,
  NM_THEME,
  SAVE
};

enum eOp
{
  SET,
  ADD
};

class Button : public AObject
{
private:
  gdl::Geometry	_geometry;
  gdl::Texture	_texture;
  gdl::Texture  _texture2;
  bool    permute;
  float		_speed;
  float		Sx;
  float		Sy;
  float		Sz;
  int     dx;
  int     fx;
  int     dy;
  int     fy;
  eVal    _key;
  eOp     _op;
  int     _val;
  int     _k;
  bool     clicked;

public:
  Button(int x, int y, int z, float _Sx, float _Sy, float _Sz);
  virtual bool initialize(float speed);
  virtual ~Button() {}
  virtual void update(gdl::Clock const &clock, gdl::Input &input);
  virtual void draw(gdl::AShader &shader, gdl::Clock const &clock);

  void    choose_text();
  void    toggle();
  int     setButton(std::string);
  int     setButton2(std::string);
  void    setZone(glm::vec4);
  void    setEffect(eVal key, eOp op, int val);
  void    setKeyboard(int);
  void    effect();
};