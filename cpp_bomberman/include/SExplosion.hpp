#ifndef SEXPLOSION_HH_
# define SEXPLOSION_HH_

#include <list>
#include <algorithm>
#include "Systems.hpp"

class SExplosion
{
public:
	SExplosion(entity *player);
	~SExplosion();
	SExplosion(SExplosion const &oth);
	SExplosion operator=(SExplosion const &oth);

	void		setExplosion();
	void		setClock(gdl::Clock const &clock);
	double	 	getClock() const;
	bool	 	getOk() const;
	void		setOk();

private:
	entity                  *player;
	t_pos                   x;
	t_pos                   y;
	std::list<entity *>     deflag;
	int                     range;
	double					time;
	bool					ok;

	bool 		setDeflag(t_pos x, t_pos y);
};

extern std::list<SExplosion *>     g_explosions;

void 			isExplodable(gdl::Clock const &clock);
SExplosion		*putBomb(entity *player);
void			clearExplosions();

#endif
