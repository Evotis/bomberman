#pragma once

#include "AObject.hpp"
#include "RButton.hpp"
#include "RModel.hpp"
#include <vector>

class Screen : public AObject
{
private:
  gdl::Geometry	_geometry;
  gdl::Texture	_texture;
  gdl::Texture  _texture2;
  bool    permute;
  float		_speed;
  float		Sx;
  float		Sy;
  float		Sz;
  std::vector<Button*> xbutton;
  Model*               marvin;

public:
  Screen(int x, int y, int z, float _Sx, float _Sy, float _Sz);
  virtual bool initialize(float speed);
  virtual ~Screen() {}
  virtual void update(gdl::Clock const &clock, gdl::Input &input);
  virtual void draw(gdl::AShader &shader, gdl::Clock const &clock);

  void    choose_text();
  void    addButton(glm::vec3 pos, glm::vec3 scale);
  void    lastButton(std::string path, glm::vec4 width);
  void    lastButton(eVal key, eOp op, int val);
  void    lastButton(int val);
  void    addMarvin(glm::vec3 pos);
  int     setScreen(std::string);
  int     setScreen2(std::string);
};
