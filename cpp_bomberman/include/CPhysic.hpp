/*
** CPhysic.hh for include in /home/mohame_e/project/cpp/cpp_bomberman/include
**
** Made by Ahmed Mohamed Ali
** Login   <mohame_e@epitech.net>
**
** Started on  Wed May 14 15:09:05 2014 Ahmed Mohamed Ali
** Last update Wed May 14 15:09:05 2014 Ahmed Mohamed Ali
*/

#ifndef CPHYSIC_HPP_
# define CPHYSIC_HPP_

#include "AComponent.hpp"

namespace Component {
  class Physic :public AComponent {
    public:
      // ctors & dtors
      Physic();
      Physic(const Physic&);
      Physic& operator=(const Physic&);
      ~Physic();
  };
}

#endif  /* !CPHYSIC_HPP_ */
