//
// CExplode.hh for include in /home/n0t/dev/cpp/bomberman/cpp_bomberman/include
//
// Made by thomas nieto
// Login   <nieto_t@epitech.net>
//
// Started on  Sat May 10 18:49:24 2014 thomas nieto
// Last update Sat May 10 18:49:24 2014 thomas nieto
//

#ifndef CEXPLODE_HPP_
# define CEXPLODE_HPP_

#include "AComponent.hpp"

namespace Component {
  class Explode :public AComponent {
    private:

    public:
      // ctors & dtors
      Explode();
      Explode(const Explode&);
      Explode& operator=(const Explode&);
      ~Explode();

      // getters
      void update();
  };
}

#endif /* !CEXPLODE_HPP_ */
