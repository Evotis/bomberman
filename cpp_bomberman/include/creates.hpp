/*
 * file:
 *
 * author: Thomas Nieto - nieto_t
 *
 * Tek_2 - project: cpp_bomberman
 */

#ifndef CREATES_HPP_
# define CREATES_HPP_

#include "CDestroyable.hpp"
#include "CDie.hpp"
#include "CExplode.hpp"
#include "CGeometry.hpp"
#include "CInventory.hpp"
#include "CKey.hpp"
#include "CLife.hpp"
#include "CPhysic.hpp"
#include "CPosition.hpp"
#include "CScore.hpp"
#include "CScreen.hpp"
#include "CScript.hpp"
#include "CSound.hpp"
#include "CAlive.hpp"
#include "CType.hpp"
#include "CIsTaken.hpp"

#endif // CREATES_HH_
