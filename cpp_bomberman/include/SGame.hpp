/*
 * file: SGame.hh
 *
 * author: Thomas Nieto - nieto_t
 *
 * Tek_2 - project: cpp_bomberman
 */

#ifndef SGAME_hh_
#define SGAME_hh_

class SGame
{
public:
	SGame();
	~SGame();
	SGame(const SGame &oth);
	SGame &operator=(const SGame &oth);
};

#endif /* !SGAME_HH_ */
