#pragma once

#include "AObject.hpp"

class Plate : public AObject
{
private:
  gdl::Geometry	_geometry;
  // gdl::Texture	_texture;
  float		_speed;

public:
  Plate(int, int, int, float, float, float);
  virtual bool initialize(float speed);
  virtual ~Plate(){}
  virtual void update(gdl::Clock const &clock, gdl::Input &input);
  virtual void draw(gdl::AShader &shader, gdl::Clock const &clock);

};
