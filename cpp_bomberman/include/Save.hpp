#ifndef SAVE_HPP_
#define SAVE_HPP_

#include <string>
#include <iostream>

bool 		save(std::string const &);
bool 	   	load(std::string file);

#endif