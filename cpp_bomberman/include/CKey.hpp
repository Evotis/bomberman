/*
** CKey.hh for include in /home/mohame_e/project/cpp/cpp_bomberman/include
**
** Made by Ahmed Mohamed Ali
** Login   <mohame_e@epitech.net>
**
** Started on  Wed May 14 15:09:05 2014 Ahmed Mohamed Ali
** Last update Wed May 14 15:09:05 2014 Ahmed Mohamed Ali
*/

#ifndef CKEY_HPP_
# define CKEY_HPP_

#include "AComponent.hpp"

enum eKey
{
  KEY_UP,
  KEY_DOWN,
  KEY_LEFT,
  KEY_RIGHT,
  KEY_DROP_BOMB,
  KEY_PAUSE,
  KEY_MENU,
  KEY_TAB
};

namespace Component {
  class Key :public AComponent {
  	std::map<eKey, int> keyboard;

    public:
      // ctors & dtors
      Key();
      Key(const Key&);
      Key& operator=(const Key&);
      ~Key();
  	 
  	 void	setKeys(std::map<eKey, int> &);
     void setKey(eKey, int);
  	 std::map<eKey, int>	&getKeys();
     int                  getKey(eKey);
  };
}

#endif  /* !CKEY_HPP_ */
