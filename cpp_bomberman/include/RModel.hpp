#pragma once

#include "AObject.hpp"
#include <Model.hh>

class Model : public AObject
{
private:
  gdl::Model    _model;
  float		_speed;
  float		Sx;
  float		Sy;
  float		Sz;
  
  int           up;
  int           down;
  int           left;
  int           right;

public:
  Model(int x, int y, int z);
  virtual bool initialize(float speed);
  virtual ~Model() {}
  virtual void update(gdl::Clock const &clock, gdl::Input &input);
  virtual void draw(gdl::AShader &shader, gdl::Clock const &clock);
  void    stop(bool);
};
