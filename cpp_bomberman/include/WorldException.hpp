//
// WorldException.hpp for include in /home/n0t/dev/cpp/bomberman/cpp_bomberman/include
//
// Made by thomas nieto
// Login   <nieto_t@epitech.net>
//
// Started on  Sun May 25 19:54:39 2014 thomas nieto
// Last update Sun May 25 19:54:39 2014 thomas nieto
//

#ifndef WORLDEXCEPTION_HPP_
# define WORLDEXCEPTION_HPP_

#include <exception>

class WorldBadLocation :public std::exception {
  virtual const char *what() const throw() {
    return "Could not locate this entity in world";
  }
} WorldBadLocation;

class WorldBadAlloc :public std::exception {
  virtual const char *what() const throw() {
    return "Position will not fit in world";
  }
} WorldBadAlloc;

#endif // !WORLDEXCEPTION_HPP_
