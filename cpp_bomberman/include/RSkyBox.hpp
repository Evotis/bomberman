#pragma once

#include "AObject.hpp"

class SkyBox : public AObject
{
private:
  gdl::Geometry	_geometry;
  float		_speed;

public:
  SkyBox(int, int, int, float, float, float, std::string);
  virtual bool initialize(float speed, int, int);
  virtual ~SkyBox(){}
  virtual void update(gdl::Clock const &clock, gdl::Input &input);
  virtual void draw(gdl::AShader &shader, gdl::Clock const &clock);
  void draw(gdl::AShader &shader, int x, int y);

};
