#ifndef SYSTEMS_HPP_
#define SYSTEMS_HPP_

#include "World.hpp"
#include "Entity.hpp"

enum Direction
{
	UP,
	DOWN,
	RIGHT,
	LEFT,
	DROP_BOMB
};

void 			kill(entity *ent);
void			setExplosion(entity *bomb);
void 			respawn(entity *player);
void			move(entity *ent, Direction dir);
void			takeBonus(entity *player, eType type);
void 			dustbin();

#endif