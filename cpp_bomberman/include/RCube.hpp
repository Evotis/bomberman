#pragma once

#include "AObject.hpp"
#include "CType.hpp"

class Cube : public AObject
{
private:
  gdl::Geometry	_geometry;
  eType         _type;
  float		_speed;
  float		Sx;
  float		Sy;
  float		Sz;

public:
  Cube(int, int, int, eType);
  virtual bool initialize(float speed);
  virtual ~Cube() {}
  virtual void update(gdl::Clock const &clock, gdl::Input &input);
  virtual void draw(gdl::AShader &shader, gdl::Clock const &clock);
};
