#ifndef CTYPE_HH_
#define CTYPE_HH_

#include "AComponent.hpp"

enum eType
{
	WALL,
	PLAYER,
	IA,
	BOX,
	DEFLAGRATION,
	BOMB,
	SPEED,
	RANGE,
	NBBOMB,
	NOTHING,
	GROUND
};

namespace Component {
	class Type : public Component::AComponent
	{
	public:
	Type(eType Type);
		~Type();
		Type(Type const &);
		Type 	&operator=(Type const &);

		eType 	geteType() const;
	private:
		eType 	type;
	};
};

#endif