//
// Plate.cpp, eyzat_f
//

#include "RPlate.hpp"

gdl::Texture _texture;
int	plat = 0;

Plate::Plate(int x, int y, int z, float sizeX, float sizeY, float sizeZ):
  AObject(x, y, z, sizeX, sizeY, sizeZ)
{
  if (plat == 0)
    {
      plat += 1;
      if (_texture.load("./sprite/texture/grass.tga") == false)
      	std::cerr << "fail texture" << std::endl;
    }
}

void Plate::update(gdl::Clock const &, gdl::Input &)
{
}

void Plate::draw(gdl::AShader &shader, gdl::Clock const &)
{
  _texture.bind();
  _geometry.draw(shader, getTransformation(), GL_QUADS);
}

bool Plate::initialize(float speed)
{
  _speed = speed;

  _geometry.setColor(glm::vec4(1, 1, 1, 1));

  _geometry.pushVertex(glm::vec3(0.5, -0.5, 0.5));
  _geometry.pushVertex(glm::vec3(0.5, -0.5, -0.5));
  _geometry.pushVertex(glm::vec3(-0.5, -0.5, -0.5));
  _geometry.pushVertex(glm::vec3(-0.5, -0.5, 0.5));

  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));

  _geometry.build();
  return (true);
}
