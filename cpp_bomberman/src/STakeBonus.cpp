#include  "World.hpp"

void		takeBonus(entity *player, eType type)
{
	if (type == SPEED)
		static_cast<Component::Inventory *>((*player)[Component::INVENTORY])->decSpeed();
	else if (type == RANGE)
		static_cast<Component::Inventory *>((*player)[Component::INVENTORY])->addRange();
	else if (type == NBBOMB)
		static_cast<Component::Inventory *>((*player)[Component::INVENTORY])->addNbBomb();
}
