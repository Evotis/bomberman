#include "SExplosion.hpp"
#include "SCollision.hpp"
#include "Entity.hpp"

std::list<SExplosion *>     g_explosions;

SExplosion::SExplosion(entity *player)
{
	x = static_cast<Component::Position *>((*player)[Component::POSITION])->getX();
	y = static_cast<Component::Position *>((*player)[Component::POSITION])->getY();
	range = static_cast<Component::Inventory *>((*player)[Component::INVENTORY])->getRange();
	this->player = player;
	time = 0;
	ok = true;
}

static void 				depop(entity *deflag)
{
  kill(g_world->pop(deflag));
}

SExplosion::~SExplosion()
{
	if (static_cast<Component::Alive *>((*player)[Component::ALIVE])->isAlive() == true || static_cast<Component::Life *>((*player)[Component::LIFE])->getLife() > 0)
    static_cast<Component::Inventory *>((*player)[Component::INVENTORY])->addNbBomb();
  static_cast<Component::Score *>((*player)[Component::SCORE])->addScore();
  std::for_each(deflag.begin(), deflag.end(), depop);
}

bool 				SExplosion::setDeflag(t_pos x, t_pos y)
{
  entity      *deflagration;
  eType       type;
  t_entlist   entlist;
  t_entlistIt it;
  bool        ret = true;
  int         collide = 0;

  deflagration = createDeflag(x, y);
  (*g_world)(x, y, entlist);

  if (g_collision.isColliding(x, y, deflagration) == true)
  {
    collide = 1;
    g_world->push(deflagration);
    this->deflag.push_front(deflagration);
    ret &= true;
  }
  // let see for list working with return
  for (it = entlist.begin(); it != entlist.end(); ++it)
  {
    if (x < g_world->getX() && y < g_world->getY())
      type = static_cast<Component::Type *>((*(*it))[Component::TYPE])->geteType();

    if (x < g_world->getX() && y < g_world->getY() && type != WALL && collide == 0)
    {
      g_world->push(deflagration);
      this->deflag.push_front(deflagration);
      ret &= false;
      collide = 1;
    }
    else if (collide == 0)
    {
      delete deflagration;
      ret &= false;
    }
  }
  return ret;
}

void SExplosion::setExplosion()
{
	int 			count = 1;
	bool 			up = true;
	bool 			down = true;
	bool 			right = true;
	bool 			left = true;
  t_entlist entlist;
  t_entlistIt it;

  (*g_world)(x, y, entlist);

  for (it = entlist.begin(); it != entlist.end(); ++it)
    if (static_cast<Component::Type *>((*(*it))[Component::TYPE])->geteType() == BOMB)
      g_world->pop(*it);

    this->setDeflag(x, y);
    while (count <= range)
    {
      if (up == true)
        up = this->setDeflag(x, y + count);
      if (down == true && static_cast<int>(y) - count >= 0)
        down = this->setDeflag(x, y - count);
      if (right == true)
        right = this->setDeflag(x + count, y);
      if (left == true && static_cast<int>(x) - count >= 0)
        left = this->setDeflag(x - count, y);
      count++;
    }
  }

  bool			SExplosion::getOk() const
  {
   return ok;
 }

 void			SExplosion::setOk()
 {
   ok = false;
 }

 void			SExplosion::setClock(gdl::Clock const &clock)
 {
   time += clock.getElapsed();
 }

 double			SExplosion::getClock() const
 {
   return time;
 }

 void        clearExplosions()
 {
  g_explosions.clear();
}

void 			isExplodable(gdl::Clock const &clock)
{
	std::list<SExplosion *>::iterator it;

	for (it = g_explosions.begin(); it != g_explosions.end(); ++it)
	{
		(*it)->setClock(clock);
		if ((*it)->getClock() >= 2.5 && (*it)->getOk() == true)
		{
			(*it)->setOk();
			(*it)->setExplosion();
		}
		else if ((*it)->getClock() >= 3 && (*it)->getOk() == false)
		{
			delete(*it);
			it = g_explosions.erase(it++);
		}
	}
}
