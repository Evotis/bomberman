//
// CSound.cc for src in /home/n0t/dev/cpp/bomberman/cpp_bomberman/src
//
// Made by thomas nieto
// Login   <nieto_t@epitech.net>
//
// Started on  Sat May 10 19:06:55 2014 thomas nieto
// Last update Fri May 23 14:44:14 2014 nelson touron
//

#include "CSound.hpp"


// ctors & dtors
Component::Sound::Sound() : Component::AComponent() {
  type_ = Component::SOUND;
}

Component::Sound::Sound(const Sound& oth) : Component::AComponent() {
  type_ = oth.type_;
}

Component::Sound& Component::Sound::operator=(const Sound& lft) {
  if (this != &lft) {
    type_ = lft.type_;
  }

  return *this;
}

Component::Sound::~Sound() {}

// getters

