//
// CPhysic.cc for src in /home/n0t/dev/cpp/bomberman/cpp_bomberman/src
//
// Made by thomas nieto
// Login   <nieto_t@epitech.net>
//
// Started on  Sat May 10 19:06:55 2014 thomas nieto
// Last update Sat May 10 19:06:55 2014 thomas nieto
//

#include "CPhysic.hpp"


// ctors & dtors
Component::Physic::Physic() : Component::AComponent() {
  type_ = Component::PHYSIC;
}

Component::Physic::Physic(const Physic& oth) : Component::AComponent() {
  type_ = oth.type_;
}

Component::Physic& Component::Physic::operator=(const Physic& lft) {
  if (this != &lft) {
    type_ = lft.type_;
  }

  return *this;
}

Component::Physic::~Physic() {}

// getters

