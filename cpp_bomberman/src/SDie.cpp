#include "Systems.hpp"

void 			kill(entity *ent)
{
  if (static_cast<Component::Type *>((*ent)[Component::TYPE])->geteType() == PLAYER
    ||  static_cast<Component::Type *>((*ent)[Component::TYPE])->geteType() == IA)
  {
    if ((static_cast<Component::Life *>((*ent)[Component::LIFE]))->getLife() - 1 > 0)
    {
     (static_cast<Component::Life *>((*ent)[Component::LIFE]))->loseLife();
     g_world->pop(ent);
     respawn(ent);
   }
   else
     (static_cast<Component::Alive *>((*ent)[Component::ALIVE]))->setDeath();
 }
 else
  (static_cast<Component::Alive *>((*ent)[Component::ALIVE]))->setDeath();
}
