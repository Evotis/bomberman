#include "CLife.hpp"

Component::Life::Life()
{
	max = 3;
	current = 3;
}

Component::Life::~Life()
{}

Component::Life::Life(Life const &oth)
: Component::AComponent()
{
	current = oth.getLife();
	max = oth.getMaxLife();
}

Component::Life 		&Component::Life::operator=(Life const &oth)
{
	if (this != &oth)
	{
		current = oth.getLife();
		max = oth.getMaxLife();
	}
	return *this;
}

void 		Component::Life::setLife(int nLife)
{
	current = nLife;
}

void 		Component::Life::loseLife()
{
	current -= 1;
}

t_life		Component::Life::getLife() const
{
	return current;
}

t_life		Component::Life::getMaxLife() const
{
	return max;
}