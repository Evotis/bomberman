
#include "RModel.hpp"

Model::Model(int x, int y, int z):
  AObject(x, y, z, 1, 1, 1), Sx(1), Sy(1), Sz(1)
{

  _model.load("./LibBomberman_linux_x64/assets/marvin.fbx");
  _scale.x = 0.003;
  _scale.y = 0.003;
  _scale.z = 0.003;
  this->_model.createSubAnim(0, "run", 36, 52);
  this->_model.setCurrentSubAnim("run", true);
  _model.pause(true);
}

void Model::update(gdl::Clock const &clock, gdl::Input &input)
{
	(void)clock;
	(void)input;
}

void Model::stop(bool state)
{
  _model.pause(state);
}

void Model::draw(gdl::AShader &shader, gdl::Clock const &clock)
{
  _model.draw(shader, getTransformation(), clock.getElapsed() * 2);
}

# define MAX(x) x / 2
# define MIN(x) (x / 2) * (-1)

bool Model::initialize(float speed)
{
  _speed = speed * 10;

  return (true);
}
