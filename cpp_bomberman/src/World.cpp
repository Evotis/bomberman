//
// Quad.cpp for test in /home/touron_n/eph/qtree
//
// Made by nelson touron
// Login   <touron_n@epitech.net>
//
// Started on  Mon May 26 18:35:54 2014 nelson touron
// Last update Sun Jun 15 16:56:23 2014 nelson touron

#include "World.hpp"

World *g_world = NULL;

World::World(const std::list<entity *> &entList, int const lnt, int const wdt,
	     int const xMark, int const yMark, int const dpt)
  : entList_(entList), lnt_(lnt), wdt_(wdt), xMark_(xMark), yMark_(yMark), dpt_(dpt)
{
  for (int cpt = 0; cpt < nbChild; cpt += 1)
    childs_[cpt] = NULL;
  initSorter();
}

World::World(int const lnt, int const wdt) 
  : lnt_(lnt), wdt_(wdt), xMark_(0), yMark_(0), dpt_(0)
{
  for (int cpt = 0; cpt < nbChild; cpt += 1)
    childs_[cpt] = NULL;
  initSorter();
}

World::~World() {}

void						World::initSorter()
{
  sorter_[0].xMin = xMark_ + 0;
  sorter_[0].xMax = xMark_ + ((lnt_ / 2) - 1);
  sorter_[0].yMin = yMark_ + 0;
  sorter_[0].yMax = yMark_ + ((wdt_ / 2) - 1);
  sorter_[1].xMin = xMark_ + (lnt_ / 2);
  sorter_[1].xMax = (xMark_ + (lnt_ / 2)) + ((lnt_ / 2) - 1);
  sorter_[1].yMin = yMark_ + 0;
  sorter_[1].yMax = yMark_ + ((wdt_ / 2) - 1);
  sorter_[2].xMin = xMark_ + 0;
  sorter_[2].xMax = xMark_ + ((lnt_ / 2) - 1);
  sorter_[2].yMin = yMark_ + (wdt_ / 2);
  sorter_[2].yMax = (yMark_ + (wdt_ / 2)) + ((wdt_ / 2) - 1);
  sorter_[3].xMin = xMark_ + (lnt_ / 2);
  sorter_[3].xMax = (xMark_ + (lnt_ / 2)) + ((lnt_ / 2) - 1);
  sorter_[3].yMin = yMark_ + (wdt_ / 2);
  sorter_[3].yMax = (yMark_ + (wdt_ / 2)) + ((wdt_ / 2) - 1);
}

void						World::split() {
  std::vector<std::list<entity *> >		myLists;
  std::list<entity *>				ul;
  std::list<entity *>				ur;
  std::list<entity *>				dl;
  std::list<entity *>				dr;
  std::list<entity *>				newEntList;
  myLists.push_back(ul);
  myLists.push_back(ur);
  myLists.push_back(dl);
  myLists.push_back(dr);

  for (std::list<entity *>::iterator it = entList_.begin(); it != entList_.end(); ++it) {
    bool					checked = false;
    for (int cpt = 0; cpt < nbChild; cpt += 1) {
      entity *tmp = *it;
      int x = static_cast<Component::Position *>((*tmp)[Component::POSITION])->getX();
      int y = static_cast<Component::Position *>((*tmp)[Component::POSITION])->getY();
      if (((x >= sorter_[cpt].xMin) && (x <= sorter_[cpt].xMax)) &&
	  ((y >= sorter_[cpt].yMin) && (y <= sorter_[cpt].yMax))) {
	checked = true;
	myLists[cpt].push_back(*it);
      }
    }
    if (checked == false) //this one stays in the father
      newEntList.push_back(*it);
  }

  entList_ = newEntList;
  int	new_lnt = lnt_ / 2;
  int	new_wdt = wdt_ / 2;
  int	new_dpt = dpt_ + 1;
  childs_[0] = new World(myLists[0], new_lnt, new_wdt, (xMark_ + 0), (yMark_ + 0), new_dpt);
  childs_[1] = new World(myLists[1], new_lnt, new_wdt, (xMark_ + new_lnt), (yMark_+ 0), new_dpt);
  childs_[2] = new World(myLists[2], new_lnt, new_wdt, (xMark_ + 0), (yMark_ + new_wdt), new_dpt);
  childs_[3] = new World(myLists[3], new_lnt, new_wdt, (xMark_ + new_lnt), (yMark_ + new_wdt), new_dpt);

  for (int cpt = 0; cpt < nbChild; cpt += 1) {
    if ((static_cast<int>(childs_[cpt]->entList_.size()) > entPerQuad) &&
	((childs_[cpt]->lnt_ != 1) && (childs_[cpt]->wdt_ != 1)))
      childs_[cpt]->split();
  }
}

void						World::push(entity * const ent) {
  int x = static_cast<Component::Position *>((*ent)[Component::POSITION])->getX();
  int y = static_cast<Component::Position *>((*ent)[Component::POSITION])->getY();

  if (static_cast<int>(entList_.size()) < entPerQuad) {
    entList_.push_back(ent);
  }
  else { //checking if sons can fit the entity
    bool					checker = false;
    for (int cpt = 0; cpt < nbChild; cpt += 1) {
      if (((x >= sorter_[cpt].xMin) && (x <= sorter_[cpt].xMax)) &&
	  ((y >= sorter_[cpt].yMin) && (y <= sorter_[cpt].yMax))) {
	checker = true;
	if (childs_[cpt] != NULL) {
	  childs_[cpt]->push(ent);
	}
	else {
	  entList_.push_back(ent);
	  this->split();
	}
      }
    }
    if (checker == false) {
      entList_.push_back(ent);
    }
  }
}

void		       			World::operator()(t_pos const xTarg, t_pos const yTarg,
							   t_entlist & acc) const {
  std::list<entity *>::const_iterator			it;
  bool							checker = false;
  for (it = entList_.begin(); it != entList_.end(); ++it) {
    entity *tmp = *it;
    t_pos x = static_cast<Component::Position *>((*tmp)[Component::POSITION])->getX();
    t_pos y = static_cast<Component::Position *>((*tmp)[Component::POSITION])->getY();
    if ((xTarg == x) && (yTarg == y)) {
      checker = true;
      acc.push_back(*it);
    }
  }

  if (checker == true)
    return;

  for (int cpt = 0; cpt < nbChild; cpt += 1) {
    if (((static_cast<int>(xTarg) >= sorter_[cpt].xMin)
    && ((static_cast<int>(xTarg) <= sorter_[cpt].xMax)))
    && ((static_cast<int>(yTarg) >= sorter_[cpt].yMin)
    && (static_cast<int>(yTarg) <= sorter_[cpt].yMax))) {
      if (childs_[cpt] != NULL)
	(*childs_[cpt])(xTarg, yTarg, acc);
    }
  }
}

entity						*World::pop(entity * const ent) {
  t_pos xTarg = static_cast<Component::Position *>((*ent)[Component::POSITION])->getX();
  t_pos yTarg = static_cast<Component::Position *>((*ent)[Component::POSITION])->getY();

  std::list<entity *>::iterator			it;
  for (it = entList_.begin(); it != entList_.end(); ++it) {
    entity	*tmp = *it;
    t_pos x = static_cast<Component::Position *>((*tmp)[Component::POSITION])->getX();
    t_pos y = static_cast<Component::Position *>((*tmp)[Component::POSITION])->getY();
    if ((xTarg == x) && (yTarg == y) && (ent == *it))
      {
	entList_.remove(*it);
	return *it;
      }
  }
  for (int cpt = 0; cpt < nbChild; cpt += 1) {
    if (((static_cast<int>(xTarg) >= sorter_[cpt].xMin)
    && ((static_cast<int>(xTarg) <= sorter_[cpt].xMax)))
    && ((static_cast<int>(yTarg) >= sorter_[cpt].yMin)
    && (static_cast<int>(yTarg) <= sorter_[cpt].yMax))) {
      if (childs_[cpt] != NULL)
	return (childs_[cpt]->pop(ent));
    }
  }
  return NULL;
}

void						World::dump(std::ostream &os) const {
  os << "[depth=" << dpt_ << "]"
     << "[x=" << xMark_ << ",y=" << yMark_ << "]"
     << "[lnt=" << lnt_ << ",wdt=" << wdt_ << "]"
     << "[nbEnt=" << entList_.size() << "]"
     << std::endl;

  for (int cpt = 0; cpt < nbChild; cpt += 1) {
    if (childs_[cpt] != NULL)
      childs_[cpt]->dump(os);
  }
}

void					       World::deleteTree() {
  if (this != NULL) {
    for (int cpt = 0; cpt < nbChild; cpt += 1) {
      childs_[cpt]->deleteTree();
    }
    for (std::list<entity *>::iterator it = entList_.begin(); it != entList_.end(); ++it) {
      deleteEntity(*it);
    }
    delete this;
  }
}

int						World::getxMark() const {return xMark_;}

int						World::getyMark() const {return yMark_;}

size_t						World::getX() const {return lnt_;}

size_t						World::getY() const {return wdt_;}

int						World::getdpt() const {return dpt_;}

const std::list<entity *>			&World::getEntList() const {return entList_;}

World						*World::getChild(int id) const {
  if ((id < 0) || (id > 3)) {
    std::cerr << "[ERROR]: Request for child with wrong ID" << std::endl;
    return NULL;
  }
  return childs_[id];
}

void						World::setX(size_t x) {lnt_ = x;}

void						World::setY(size_t y) {wdt_ = y;}

std::ostream					&operator<<(std::ostream &lft, World &rht) {
  rht.dump(lft);
  return lft;
}
