//
// SRender.cpp for bomberman
//

#include <fstream>
#include <sstream>
#include "SRender.hpp"
#include "SExplosion.hpp"
#include "SCollision.hpp"
#include "Save.hpp"
#include "Systems.hpp"
#include "SaveError.hpp"

float TX = 1440.0f;
float TY = 720.0f;

int   radius = 10;

extern std::map<eType, gdl::Texture> m_texture;
extern std::map<eVal, int> g_config;

SRender::SRender(int baseX, int baseY, gdl::SdlContext & context, std::string theme) : X(baseX), Y(baseY)
{
  sky = new SkyBox(1, 1, 1, 1, 1, 1, theme);

  eSpeed = 1400;
  sky->initialize(10.0f, baseX, baseY);
  initialize(context);
  m_texture[GROUND].load("./" + theme + "/texture/ground.tga");
  m_texture[WALL].load("./" + theme + "/texture/wall.tga");
  m_texture[BOX].load("./" + theme + "/texture/box.tga");
  m_texture[BOMB].load("./" + theme + "/texture/tnt.tga");
  m_texture[SPEED].load("./" + theme + "/texture/bonusSpeed.tga");
  m_texture[RANGE].load("./" + theme + "/texture/bonusRange.tga");
  m_texture[NBBOMB].load("./" + theme + "/texture/bonusBomb.tga");
  m_texture[IA].load("./" + theme + "/texture/ia.tga");
  m_texture[PLAYER].load("./" + theme + "/texture/player.tga");
  m_texture[DEFLAGRATION].load("./" + theme + "/texture/explosion.tga");
  NOGROUND = false;
  if (g_config[NO_GROUND])
    NOGROUND = true;
  g_config[PLAYING] = 1;
  g_config[SAVE] = 0;
}

SRender::~SRender()
{
}

bool SRender::start()
{
  fillMap();
  
  multi = false;
  nbPlayer = _player.size();
  if (nbPlayer > 1)
    multi = true;
  while (g_config[START] != 0)
    {
      while (g_config[PLAYING] == 1)
	{
	  playing();
	  if (nbPlayer == 0 && multi == false)
	    g_config[PLAYING] = 0;
	  else if (nbPlayer == 1 && multi == true)
	    g_config[PLAYING] = 0;
	}
      in_pause();
    }
  saveScore();
  g_world->deleteTree();
  clearExplosions();
  return (true);
}

void SRender::in_pause()
{
  glViewport(0, 0, TX, TY);
  glm::mat4 transformation;
  projection = glm::perspective(30.0f, TX / TY, 0.01f, 100.0f);
  transformation = glm::lookAt(glm::vec3(0, 0, -30), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
  _shader.bind();
  _shader.setUniform("view", transformation);
  _shader.setUniform("projection", projection);
  while (g_config[PLAYING] == 0 && g_config[START] != 0)
  {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);   
    _pause->draw(_shader, _clock);
    _context.flush();
    updateP();
    if (g_config[SAVE] == 1)
    {
      save("save1.txt");
      g_config[SAVE] = 0;
    }
  }
}

void SRender::playing()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  for (unsigned int i = 0; i < _player.size(); ++i)
  {
    camEntity(_player[i]);
    mydraw(_player[i]);
    update();
  }
  _context.flush();
}

int           getnbr(std::string line)
{
  int         value;

  std::istringstream buffer(line);
  buffer >> value;
  return value;
}

void                              SRender::saveScore()
{
  t_entlist                       entlist;
  t_entlistIt                     it;
  std::string                     line;
  std::vector<int>                values;
  std::ifstream                   ffile("highscore.txt");
  std::ofstream                   flux;
  int                             score;

  for (int x = 0; x < X; x++)
  {
    for (int y = 0; y < Y; y++)
    {
      entlist.clear();
      (*g_world)(x, y, entlist);
      for (it = entlist.begin(); it != entlist.end(); ++it)
      {
        if (static_cast<Component::Type *>((*(*it))[Component::TYPE])->geteType() != PLAYER)
          continue;
        score = static_cast<Component::Score *>((*(*it))[Component::SCORE])->getScore();
      }
    }
  }

  try
  {
    if (!ffile.is_open())
      throw SaveError("Wrong file name");
  }
  catch (std::exception const &e)
  {
    std::cout << e.what() << std::endl;
    return;
  }
  
  while (getline(ffile, line))
    values.push_back(getnbr(line));

  values.push_back(score);
  flux.open("highscore.txt");

  try
  {
    if (!flux)
      throw SaveError("Wrong file name");
  }
  catch (std::exception const &e)
  {
    std::cout << e.what() << std::endl;
    return;
  }

  std::cout << "--------------" << std::endl;
  for (std::vector<int>::reverse_iterator ite = values.rbegin(); ite != values.rend(); ++ite)
  {
    std::cout << (*ite) << std::endl;
    flux << (*ite) << std::endl;
  }
  return;
}

bool SRender::updateP()
{
 if (_input.getKey(SDLK_ESCAPE) || _input.getInput(SDL_QUIT))
  g_config[START] = 0;
_pause->update(_clock, _input);
_context.updateClock(_clock);
_context.updateInputs(_input);
return (true);
}

void SRender::mydraw(entity *obj)
{
  int a, b;
  glm::vec3 pos;
  t_entlist   entlist;
  t_entlistIt it;

  a = static_cast<Component::Position *>((*obj)[Component::POSITION])->getX();
  b = static_cast<Component::Position *>((*obj)[Component::POSITION])->getY();
  sky->draw(_shader, a, b);
  for (int x = -1; x <= X; x++)
  {
    for (int y = -1; y <= Y; y++)
    {
     if ((x > a - radius && x < a + radius) && (y > b - radius && y < b + radius))
     {
       if (NOGROUND == false)
       {
        allstar[GROUND]->setPosition(x, -1, y, 0);
        allstar[GROUND]->draw(_shader, _clock);
      }
      if (x == -1 || y == -1 || x == X || y == Y)
      {
        allstar[WALL]->setPosition(x, 0, y, 0);
        allstar[WALL]->draw(_shader, _clock);
      }
      else
      {
        entlist.clear();
        (*g_world)(x, y, entlist);
        for (it = entlist.begin(); it != entlist.end(); ++it)
        {
          if (static_cast<Component::Alive *>((*(*it))[Component::ALIVE])->isAlive())
           drawEntity(*it);
       }
     }
   }
 }
}
}


void      SRender::drawEntity(entity *obj)
{
  int     x;
  int     y;
  eType   t;

  t = static_cast<Component::Type *>((*obj)[Component::TYPE])->geteType();
  if (t == PLAYER || t == IA)
  {
    static_cast<Component::Geometry *>((*obj)[Component::GEOMETRY])->draw(_shader, _clock);
    return;
  }
  x = static_cast<Component::Position *>((*obj)[Component::POSITION])->getX();
  y = static_cast<Component::Position *>((*obj)[Component::POSITION])->getY();
  allstar[t]->setPosition(x, 0, y, 0);
  allstar[t]->draw(_shader, _clock);
}

void SRender::camEntity(entity *obj)
{
  glm::mat4 transformation;
  std::map<eKey, int> k;

  static_cast<Component::Screen *>((*obj)[Component::SCREEN])->window();
  cam.x = static_cast<Component::Position *>((*obj)[Component::POSITION])->getX();
  cam.z = static_cast<Component::Position *>((*obj)[Component::POSITION])->getY();
  if (_player.size() != 1)
    projection = glm::perspective(30.0f, (TX / 2) / TY, 0.01f, 100.0f);
  transformation = glm::lookAt(glm::vec3(cam.x, cam.y, cam.z + 10), glm::vec3(cam.x, 0, cam.z), lok);
  _shader.bind();
  _shader.setUniform("view", transformation);
  _shader.setUniform("projection", projection);
}


bool SRender::initialize(gdl::SdlContext & context)
{
  _context = context;
  glEnable(GL_DEPTH_TEST);
  if (!_shader.load("./LibBomberman_linux_x64/shaders/basic.fp", GL_FRAGMENT_SHADER)
    || !_shader.load("./LibBomberman_linux_x64/shaders/basic.vp", GL_VERTEX_SHADER)
    || !_shader.build())
  {
    std::cerr << "load failed" << std::endl;
    return (false);
  }
  cam.x = 0;
  cam.y = 20;
  cam.z = 0;
  lok.x = 0;
  lok.y = 1;
  lok.z = 0;
  projection = glm::perspective(30.0f, TX / TY, 0.01f, 100.0f);

  glm::mat4 transformation;

  transformation = glm::lookAt(glm::vec3(cam.x, cam.y, cam.z + 10), glm::vec3(0, 0, 0), lok);
  _shader.bind();
  _shader.setUniform("view", transformation);
  _shader.setUniform("projection", projection);


  allstar[GROUND] = new Cube(0, 0, 0, GROUND);
  allstar[GROUND]->initialize(10.0f);

  allstar[WALL] = new Cube(0, 0, 0, WALL);
  allstar[WALL]->initialize(10.0f);

  allstar[BOX] = new Cube(0, 0, 0, BOX);
  allstar[BOX]->initialize(10.0f);

  allstar[BOMB] = new Cube(0, 0, 0, BOMB);
  allstar[BOMB]->initialize(10.0f);

  allstar[DEFLAGRATION] = new Cube(0, 0, 0, DEFLAGRATION);
  allstar[DEFLAGRATION]->initialize(10.0f);

  allstar[SPEED] = new Cube(0, 0, 0, SPEED);
  allstar[SPEED]->initialize(10.0f);

  allstar[RANGE] = new Cube(0, 0, 0, RANGE);
  allstar[RANGE]->initialize(10.0f);

  allstar[NBBOMB] = new Cube(0, 0, 0, NBBOMB);
  allstar[NBBOMB]->initialize(10.0f);

  allstar[IA] = new Cube(0, 0, 0, IA);
  allstar[IA]->initialize(10.0f);

  allstar[PLAYER] = new Cube(0, 0, 0, PLAYER);
  allstar[PLAYER]->initialize(10.0f);

  _pause = new Screen(0, 0, 0, 13, 26, 13);
  _pause->setScreen("./sprite/screen/pause.tga");
  _pause->initialize(10.0f);


  _pause->addButton(glm::vec3(0, 7, 3), glm::vec3(5, 2.3, 1));
  _pause->lastButton("continue", glm::vec4(666, 777, 153, 180));
  _pause->lastButton(PLAYING, SET, 1);

  _pause->addButton(glm::vec3(0, 7, 2), glm::vec3(5, 2.3, 1));
  _pause->lastButton("save", glm::vec4(666, 777, 221, 243));
  _pause->lastButton(SAVE, SET, 1);

  _pause->addButton(glm::vec3(0, 7, 1), glm::vec3(5, 2.3, 1));
  _pause->lastButton("leave", glm::vec4(666, 777, 285, 307));
  _pause->lastButton(START, SET, 0);
  nbPlayer = _player.size();
  return (true);
}

bool scape = false;

bool SRender::update()
{
  if (_input.getKey(SDLK_p))
    g_config[PLAYING] = 0;
  if (_input.getKey(SDLK_ESCAPE))
    scape = true;
  if (!_input.getKey(SDLK_ESCAPE) && scape)
  {
    g_config[PLAYING] = 0;
    scape = false;
  }
  isExplodable(_clock);
  dustbin();
  for (unsigned int i = 0; i < _player.size(); ++i)
    updateEntity(_player[i]);
  _context.updateClock(_clock);
  _context.updateInputs(_input);
  return (true);
}

bool SRender::fillMap()
{
  t_entlist   entlist;
  t_entlistIt it;
  unsigned    int nb_player;

  for (int x = 0; x < X; x++)
  {
    for (int y = 0; y < Y; y++)
    {
      entlist.clear();
      (*g_world)(x, y, entlist);
      for (it = entlist.begin(); it != entlist.end(); ++it)
      {
        if (static_cast<Component::Type *>((*(*it))[Component::TYPE])->geteType() != PLAYER)
          continue;
        if (static_cast<Component::Alive *>((*(*it))[Component::ALIVE])->isAlive())
          _player.push_back(*it);
      }
    }
  }
  nb_player = _player.size();
  for (unsigned int i = 0; i < nb_player; ++i)
    useWindow(_player[i], i, nb_player);
  return (true);
}

void  SRender::useWindow(entity *obj, unsigned int player_num, unsigned int max_player)
{
  glm::vec4 winPl(0, 0, TX, TY);
  std::map<eKey, int> keys;

  switch (player_num)
  {
    case 1:
    keys[KEY_UP] = SDLK_z;
    keys[KEY_DOWN] = SDLK_s;
    keys[KEY_LEFT] = SDLK_q;
    keys[KEY_RIGHT] = SDLK_d;
    keys[KEY_DROP_BOMB] = SDLK_LSHIFT;
    break;
    default:
    keys[KEY_UP] = SDLK_UP;
    keys[KEY_DOWN] = SDLK_DOWN;
    keys[KEY_LEFT] = SDLK_LEFT;
    keys[KEY_RIGHT] = SDLK_RIGHT;
    keys[KEY_DROP_BOMB] = SDLK_SPACE;
    break;
  }
  if (max_player > 1)
  {
    winPl.x = (TX / max_player) * (1 - player_num);
    winPl.y = 0;
    winPl.z = TX / max_player;
    winPl.w = TY;
  }
  static_cast<Component::Screen *>((*obj)[Component::SCREEN])->setScreen(winPl);
  static_cast<Component::Key *>((*obj)[Component::KEY])->setKeys(keys);
}

void      SRender::updateEntity(entity *obj)
{
  glm::vec3 pos;
  SExplosion *expl;
  std::map<eKey, int> k;
  bool t;

  t = static_cast<Component::Inventory *>((*obj)[Component::INVENTORY])->canMove();
  static_cast<Component::Geometry *>((*obj)[Component::GEOMETRY])->stop(true);
  k = static_cast<Component::Key *>((*obj)[Component::KEY])->getKeys();

  if (t) {
    if (_input.getKey(k[KEY_DROP_BOMB]))
    {
     static_cast<Component::Inventory *>((*obj)[Component::INVENTORY])->reset();
     expl = putBomb(obj);
     if (expl != NULL)
       g_explosions.push_back(expl);
   }
   else if (_input.getKey(k[KEY_RIGHT]))
   {
     static_cast<Component::Inventory *>((*obj)[Component::INVENTORY])->reset();
     actionEntity(obj, 1, 0, 90);
   }
   else if (_input.getKey(k[KEY_DOWN]))
   {
     static_cast<Component::Inventory *>((*obj)[Component::INVENTORY])->reset();
     actionEntity(obj, 0, 1, 0);
   }
   else if (_input.getKey(k[KEY_UP]))
   {
     static_cast<Component::Inventory *>((*obj)[Component::INVENTORY])->reset();
     actionEntity(obj, 0, -1, 180);
   }
   else if (_input.getKey(k[KEY_LEFT]))
   {
     static_cast<Component::Inventory *>((*obj)[Component::INVENTORY])->reset();
     actionEntity(obj, -1, 0, 270);
   }}
   static_cast<Component::Inventory *>((*obj)[Component::INVENTORY])->addTime(static_cast<float>(_clock.getElapsed()));
  //
  // Bomb catching
  //
  // pressed
 }


void      SRender::actionEntity(entity *obj, int x, int z, int rotation)
{
  glm::vec3 pos(0, 0, 0);

  (void)rotation;
  pos.x = static_cast<Component::Position *>((*obj)[Component::POSITION])->getX() + x;
  pos.z = static_cast<Component::Position *>((*obj)[Component::POSITION])->getY() + z;
  if (pos.x < 0 || pos.z < 0 || pos.x > X || pos.z > Y)
    return;
  if (static_cast<Component::Alive *>((*obj)[Component::ALIVE])->isAlive() == false)
    nbPlayer -= 1;
  if (g_collision.isColliding(pos.x, pos.z, obj))
  {
    g_world->pop(obj);
    static_cast<Component::Geometry *>((*obj)[Component::GEOMETRY])->pos(pos, rotation);
    static_cast<Component::Position *>((*obj)[Component::POSITION])->setPos(pos.x, pos.z);
    g_world->push(obj);
  }
  static_cast<Component::Geometry *>((*obj)[Component::GEOMETRY])->stop(false);
}

void      SRender::cubeEntity(entity *obj)
{

  int type;
  type = static_cast<Component::Type *>((*obj)[Component::TYPE])->geteType();
  if (type == PLAYER)
    _player.push_back(obj);
}

bool SRender::initialize()
{
  return (true);
}
