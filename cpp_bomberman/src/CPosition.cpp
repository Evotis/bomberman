/*
** CPosition.cpp for src in /home/mohame_e/project/cpp/cpp_bomberman/src
**
** Made by Ahmed Mohamed Ali
** Login   <mohame_e@epitech.net>
**
** Started on  Wed May 14 15:12:06 2014 Ahmed Mohamed Ali
** Last update Wed May 14 15:12:06 2014 Ahmed Mohamed Ali
*/

#include "CPosition.hpp"

Component::Position::Position(int x, int y)
: x_(x), y_(y) {
	type_ = Component::POSITION;
}

Component::Position::Position(const Position& oth)
: Component::AComponent() {
  type_ = oth.type_;
}

Component::Position::~Position() {}

Component::Position& Component::Position::operator=(const Position& oth) {
  if (this != &oth) {
    x_ = oth.x_;
    y_ = oth.y_;
  }

  return (*this);
}

t_pos Component::Position::getX() const {
  return x_;
}

t_pos Component::Position::getY() const {
  return y_;
}

void Component::Position::setPos(int x, int y) {
  if (x > none)
    x_ = static_cast<t_pos>(x);
  if (y > none)
    y_ = static_cast<t_pos>(y);
}

t_pos  Component::Position::getStartX() const
{
  return s_x;
}

t_pos  Component::Position::getStartY() const
{
  return s_y;
}