#include "CAlive.hpp"

Component::Alive::Alive(bool val)
: a(val)
{}

Component::Alive::~Alive()
{}

Component::Alive::Alive(Alive const &oth)
: Component::AComponent()
{
	a = oth.isAlive();
}

Component::Alive	&Component::Alive::operator=(Alive const &oth)
{
	if (this != &oth)
		a = oth.isAlive();
	return *this;
}

bool 			Component::Alive::isAlive() const
{
	return a;
}

void 			Component::Alive::setRebirth()
{
	a = !a;
}

void 			Component::Alive::setDeath()
{
	a = false;
}
