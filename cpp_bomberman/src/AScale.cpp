//
// CScale.cc for src in /home/n0t/dev/cpp/bomberman/cpp_bomberman/src
//
// Made by thomas nieto
// Login   <nieto_t@epitech.net>
//
// Started on  Sat May 10 19:06:55 2014 thomas nieto
// Last update Sat May 10 19:06:55 2014 thomas nieto
//

#include "CScale.hpp"


// ctors & dtors
Component::Scale::Scale() : Component::AComponent() {
  type_ = Component::Scale;
}

Component::Scale::Scale(const Scale& oth) : Component::AComponent() {
  type_ = oth.type_;
}

Component::Scale& Component::Scale::operator=(const Scale& lft) {
  if (this != &lft) {
    type_ = lft.type_;
  }

  return *this;
}

Component::Scale::~Scale() {}

// getters

gdl::vec3 	Component::Scale::getScale()
{
	return (_scale);
}

void			setScale(glm::vec3 scale)
{
	_scale = scale;
}
