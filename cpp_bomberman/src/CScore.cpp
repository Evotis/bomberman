//
// CScore.cc for src in /home/n0t/dev/cpp/bomberman/cpp_bomberman/src
//
// Made by thomas nieto
// Login   <nieto_t@epitech.net>
//
// Started on  Sat May 10 19:06:55 2014 thomas nieto
// Last update Sat May 10 19:06:55 2014 thomas nieto
//

#include "CScore.hpp"


// ctors & dtors
Component::Score::Score() : Component::AComponent() {
	type_ = Component::SCORE;
}

Component::Score::Score(const Score& oth) : Component::AComponent() {
	type_ = oth.type_;
}

Component::Score& Component::Score::operator=(const Score& lft) {
	if (this != &lft) {
		type_ = lft.type_;
	}

	return *this;
}

Component::Score::~Score() {}

void	Component::Score::setScore(int nScore)
{
	score = nScore;
}

void	Component::Score::addScore()
{
	score += 100;
}

int		Component::Score::getScore() const
{
	return score;
}
