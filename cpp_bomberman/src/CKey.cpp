//
// CKey.cc for src in /home/n0t/dev/cpp/bomberman/cpp_bomberman/src
//
// Made by thomas nieto
// Login   <nieto_t@epitech.net>
//
// Started on  Sat May 10 19:06:55 2014 thomas nieto
// Last update Sat May 10 19:06:55 2014 thomas nieto
//

#include "CKey.hpp"


// ctors & dtors
Component::Key::Key() : Component::AComponent() {
  type_ = Component::KEY;
}

Component::Key::Key(const Key& oth) : Component::AComponent() {
  type_ = oth.type_;
}

Component::Key& Component::Key::operator=(const Key& lft) {
  if (this != &lft) {
    type_ = lft.type_;
  }

  return *this;
}

Component::Key::~Key() {}

// getters

void	Component::Key::setKeys(std::map<eKey, int> &kl)
{
	keyboard = kl;
}


void	Component::Key::setKey(eKey k, int touche)
{
	keyboard[k] = touche;
}

std::map<eKey, int> &Component::Key::getKeys()
{
	return keyboard;
}

int		Component::Key::getKey(eKey k)
{
	return (keyboard[k]);
}
