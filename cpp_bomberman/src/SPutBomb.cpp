#include "Systems.hpp"
#include "SExplosion.hpp"

SExplosion 			*putBomb(entity *player)
{
  if ((static_cast<Component::Type *>((*player)[Component::TYPE]))->geteType() == PLAYER && static_cast<Component::Alive *>((*player)[Component::ALIVE])->isAlive())
  {
    t_pos 	    x = (static_cast<Component::Position *>((*player)[Component::POSITION]))->getX();
    t_pos       y = (static_cast<Component::Position *>((*player)[Component::POSITION]))->getY();
    t_pos       range = (static_cast<Component::Inventory *>((*player)[Component::INVENTORY]))->getRange();
    entity 	    *Bomb = createBomb(x, y);
    t_entlist   entlist;
    t_entlistIt it;

    (*g_world)(x, y, entlist);
    for (it = entlist.begin(); it != entlist.end(); ++it)
      if (static_cast<Component::Type *>((*(*it))[Component::TYPE])->geteType() == BOMB)
        return NULL;
    (static_cast<Component::Inventory *>((*Bomb)[Component::INVENTORY]))->setRange(range);
    // std::cout << (static_cast<Component::Inventory *>((*player)[Component::INVENTORY]))->getNbBomb() << std::endl;
    if ((static_cast<Component::Inventory *>((*player)[Component::INVENTORY]))->getNbBomb() >= 1)
    {
     SExplosion *expl = new SExplosion(player);
     (static_cast<Component::Inventory *>((*player)[Component::INVENTORY]))->subNbBomb();
     g_world->push(Bomb);
     return expl;
   }
   return NULL;
 }
 return NULL;
}
