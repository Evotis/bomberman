#include "CType.hpp"

Component::Type::Type(eType ntype)
{
	 type = ntype;
}

Component::Type::~Type()
{}

Component::Type::Type(Type const &oth)
: Component::AComponent()
{
	type = oth.geteType();
}

Component::Type 		&Component::Type::operator=(Type const &oth)
{
	if (&oth != this)
		type = oth.geteType();
	return *this;
}

eType 	Component::Type::geteType() const
{
	return type;
}