#include <sstream>
#include "SMenu.hpp"
#include "SExplosion.hpp"

int vz = -30;
int vy = 0;
int vx = 0;

std::map<eVal, int> g_config;

Menu::Menu(int baseX, int baseY)
: X(baseX), Y(baseY)
{
}

Menu::~Menu()
{
}

bool Menu::start()
{
	if (initialize() == false)
		return (false);
	while (update() == true)
	{
		if (g_config[START] != 0)
			launch_game(g_config[START]);
		glViewport(0, 0, 1440, 720);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		draw();
		_context.flush();
	}
	return (true);
}

int theme_val = 0;

void Menu::draw()
{
	glm::mat4 transformation;
	transformation = glm::lookAt(glm::vec3(vx, vy, vz), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	_shader.bind();
	_shader.setUniform("view", transformation);
	_shader.setUniform("projection", projection);
	_screens[static_cast<ePage>(g_config[PAGE])]->draw(_shader, _clock);
	if (static_cast<ePage>(g_config[PAGE]) == OPTION)
		draw_numbers();
	else if (static_cast<ePage>(g_config[PAGE]) == HIGHSCORE)
		draw_score();
	else if (static_cast<ePage>(g_config[PAGE]) == THEMES)
		{
			limit(NM_THEME, 0, 2);
			if (theme_val != g_config[NM_THEME])
			{
				theme_val = g_config[NM_THEME];
				_screens[THEMES]->setScreen("./" + _texturepack[theme_val] + "/fond.tga");
			}
		}
}

bool Menu::initialize()
{
	if (!_context.start(1440, 720, "Bomberman!"))
	{
		std::cerr << "start failed" << std::endl;
		return (false);
	}
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	if (!_shader.load("./LibBomberman_linux_x64/shaders/basic.fp", GL_FRAGMENT_SHADER)
		|| !_shader.load("./LibBomberman_linux_x64/shaders/basic.vp", GL_VERTEX_SHADER)
		|| !_shader.build())
	{
		std::cerr << "load failed" << std::endl;
		return (false);
	}
	projection = glm::perspective(30.0f, 1440.0f / 720.0f, 0.01f, 100.0f);
	initializeMenu();
	nb();
	g_config[PAGE] = static_cast<int>(OPENING);
	g_config[WIDTH] = 20;
	g_config[HEIGHT] = 20;
	g_config[START] = 0;
	g_config[NB_BOX] = 40;
	g_config[NB_IA] = 0;
	g_config[BONUS] = 20;
	g_config[PLAYING] = 0;
	g_config[NO_GROUND] = 0;
	g_config[NM_THEME] = 0;
	_texturepack[0] = "sprite";
	_texturepack[1] = "sprite_tron";
	_texturepack[2] = "sprite_hypster";
	return (true);
}

void Menu::limit(eVal t, int min, int max)
{
	if (g_config[t] < min)
		g_config[t] = max;
	
	if (g_config[t] > max)
		g_config[t] = min;
}

bool Menu::launch_game(int i)
{
	(void)i;
	t_pos x = g_config[WIDTH];
	t_pos y = g_config[HEIGHT];

	if (x % 2 == 0)
		x++;
	if (y % 2 == 0)
		y++;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	_screens[LOADING]->draw(_shader, _clock);
	_context.flush();

	switch (g_config[START])
	{
		case 1:
		if (!load("savepac.txt"))
		{
			g_config[START] = 0;
			return 0;
		}
		break;
		case 2:
		if (!load("map2.txt"))
		{
			g_config[START] = 0;
			return 0;
		}
		break;
		case 3:
		if (!load("map3.txt"))
		{
			g_config[START] = 0;
			return 0;
		}
		break;
		case 4:
		if (!load("map4.txt"))
		{
			g_config[START] = 0;
			return 0;
		}
		break;
		case 5:
		if (!load("map5.txt"))
		{
			g_config[START] = 0;
			return 0;
		}
		break;
		case 6:
		try {
			Generator gen(x, y, static_cast<float>(g_config[NB_BOX]) / 100,
				static_cast<float>(g_config[NB_IA]) / 100,
				static_cast<float>(g_config[BONUS]) / 100);
			std::cerr << gen << g_world << std::endl;

			g_world = new World(gen.get(), x, y, 0, 0, 0);
			g_world->split();
		}
		catch (std::exception const &e)
		{
			std::cerr << e.what() << std::endl;
			    // need a better handle here like a loop who prevent user to fuck
			    // with the program again
			return false;
		}
		break;
		case 7:
		if (!load("save1.txt"))
		{
			g_config[START] = 0;
			return 0;
		}
		break;
	}

	if (g_config[START] != 7)
	{
		g_world->push(createPlayer(0, 0));
		if (g_config[PAGE] == static_cast<int>(MULTI))
			g_world->push(createPlayer(g_world->getX() - 1, g_world->getY() - 1));
	}
	SRender render(g_world->getX(), g_world->getY(), _context, _texturepack[g_config[NM_THEME]]);
	render.start();
	g_config[START] = 0;		
	return (true);
}

bool Menu::update()
{
	if (_input.getKey(SDLK_ESCAPE) || _input.getInput(SDL_QUIT)) {
		return (false);
	}
	_context.updateClock(_clock);
	_context.updateInputs(_input);
	_screens[static_cast<ePage>(g_config[PAGE])]->update(_clock, _input);
	return (true);
}

bool Menu::addPict(std::string path, ePage p, bool dual)
{
	Screen *opening = new Screen(0, 0, 0, 13, 26, 13);
	opening->setScreen("./sprite/screen/" + path + ".tga");
	if (dual)
		opening->setScreen2("./sprite/screen/" + path + "2.tga");
	opening->initialize(10.0f);
	_screens[p] = opening;
	return (true);
}

bool Menu::nb()
{
	for (int i = 0; i < 10; i++)
	{
		std::stringstream ss;
		ss << i;
		std::string str = ss.str();

		Screen *opening = new Screen(0, 0, 0, 1, 1, 2);
		opening->setScreen("./sprite/numbers/" + str + ".tga");
		opening->initialize(10.0f);
		_numbers[i] = opening;
	}
	return (true);
}

void Menu::draw_score()
{
	std::ifstream score;
	score.open("highscore.txt");
	int x = 4;

	for (std::string line; std::getline(score, line);)
	{
		std::istringstream buffer(line);
		int value;
		buffer >> value;
		printValue(value, glm::vec3(-1, 7, x));
		x -= 2;
	}
}

void Menu::draw_numbers()
{
	limit(NB_BOX, 0, 100);
	limit(NB_IA, 0, 100);
	limit(BONUS, 0, 100);
	limit(WIDTH, 5, 100);
	limit(HEIGHT, 5, 100);
	limit(NM_THEME, 0, 1);
	printValue(g_config[NB_BOX], glm::vec3(-3, 7, 3));
	printValue(g_config[NB_IA], glm::vec3(-3, 7, 1));
	printValue(g_config[BONUS], glm::vec3(-3, 7, -1));
	printValue(g_config[WIDTH], glm::vec3(-3, 7, -3));
	printValue(g_config[HEIGHT], glm::vec3(-3, 7, -5));
}

void Menu::printValue(int value, glm::vec3 pos)
{
	int x = pos.x;

	if (value == 0)
	{
		_numbers[0]->setPosition(x, pos.y, pos.z, 0);
		_numbers[0]->draw(_shader, _clock);
	}
	for (;value > 0; value /= 10)
	{
		_numbers[value % 10]->setPosition(x, pos.y, pos.z, 0);
		_numbers[value % 10]->draw(_shader, _clock);
		x++;
	}
}

void Menu::initializeMenu()
{
	glm::vec3 sc(5, 2.3, 1);

	addPict("opening", OPENING, false);

	_screens[OPENING]->addButton(glm::vec3(0, 0, -6), glm::vec3(10, 6, 3));
	_screens[OPENING]->lastButton("opening", glm::vec4(570, 880, 670, 700));
	_screens[OPENING]->lastButton(PAGE, SET, MENU);
	_screens[OPENING]->lastButton(SDLK_RETURN);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	_screens[OPENING]->draw(_shader, _clock);
	_context.flush();

	addPict("menu", MENU, false);

	_screens[MENU]->addButton(glm::vec3(9, 7, 5), sc);
	_screens[MENU]->lastButton("singleplayer", glm::vec4(75, 190, 25, 50));
	_screens[MENU]->lastButton(PAGE, SET, SINGLE);

	_screens[MENU]->addButton(glm::vec3(9, 7, 4), sc);
	_screens[MENU]->lastButton("multiplayer", glm::vec4(75, 190, 90, 115));
	_screens[MENU]->lastButton(PAGE, SET, MULTI);

	_screens[MENU]->addButton(glm::vec3(9, 7, 3), sc);
	_screens[MENU]->lastButton("load", glm::vec4(75, 190, 155, 180));
	_screens[MENU]->lastButton(START, SET, 7);

	_screens[MENU]->addButton(glm::vec3(9, 7, 2), sc);
	_screens[MENU]->lastButton("option", glm::vec4(75, 190, 220, 245));
	_screens[MENU]->lastButton(PAGE, SET, OPTION);

	_screens[MENU]->addButton(glm::vec3(9, 7, 1), sc);
	_screens[MENU]->lastButton("highscore", glm::vec4(75, 190, 285, 310));
	_screens[MENU]->lastButton(PAGE, SET, HIGHSCORE);

	_screens[MENU]->addButton(glm::vec3(9, 7, 0), sc);
	_screens[MENU]->lastButton("credits", glm::vec4(75, 190, 350, 375));
	_screens[MENU]->lastButton(PAGE, SET, CREDITS);

	_screens[MENU]->addButton(glm::vec3(9, 7, -1), sc);
	_screens[MENU]->lastButton("themes", glm::vec4(75, 190, 415, 440));
	_screens[MENU]->lastButton(PAGE, SET, THEMES);

	addPict("start_with_preview", SINGLE, false);

	_screens[SINGLE]->addMarvin(glm::vec3(17, -12, 16));

	_screens[SINGLE]->addButton(glm::vec3(9, 7, 5), sc);
	_screens[SINGLE]->lastButton("map1", glm::vec4(40, 220, 25, 260));
	_screens[SINGLE]->lastButton(START, SET, 1);

	_screens[SINGLE]->addButton(glm::vec3(6, 7, 5), sc);
	_screens[SINGLE]->lastButton("map2", glm::vec4(240, 420, 25, 260));
	_screens[SINGLE]->lastButton(START, SET, 2);

	_screens[SINGLE]->addButton(glm::vec3(3, 7, 5), sc);
	_screens[SINGLE]->lastButton("map3", glm::vec4(435, 620, 25, 260));
	_screens[SINGLE]->lastButton(START, SET, 3);

	_screens[SINGLE]->addButton(glm::vec3(0, 7, 5), sc);
	_screens[SINGLE]->lastButton("map4", glm::vec4(635, 815, 25, 260));
	_screens[SINGLE]->lastButton(START, SET, 4);

	_screens[SINGLE]->addButton(glm::vec3(-3, 7, 5), sc);
	_screens[SINGLE]->lastButton("map5", glm::vec4(830, 1010, 25, 260));
	_screens[SINGLE]->lastButton(START, SET, 5);	

	_screens[SINGLE]->addButton(glm::vec3(-6, 7, 5), sc);
	_screens[SINGLE]->lastButton("random", glm::vec4(1030, 1210, 25, 260));
	_screens[SINGLE]->lastButton(START, SET, 6);

	_screens[SINGLE]->addButton(glm::vec3(9, 7, -5), sc);
	_screens[SINGLE]->lastButton("back", glm::vec4(75, 185, 680, 705));
	_screens[SINGLE]->lastButton(PAGE, SET, MENU);

	addPict("start_with_preview", MULTI, false);

	_screens[MULTI]->addMarvin(glm::vec3(17, -12, 16));

	_screens[MULTI]->addButton(glm::vec3(9, 7, 5), sc);
	_screens[MULTI]->lastButton("map1", glm::vec4(40, 220, 25, 260));
	_screens[MULTI]->lastButton(START, SET, 1);

	_screens[MULTI]->addButton(glm::vec3(6, 7, 5), sc);
	_screens[MULTI]->lastButton("map2", glm::vec4(240, 420, 25, 260));
	_screens[MULTI]->lastButton(START, SET, 2);

	_screens[MULTI]->addButton(glm::vec3(3, 7, 5), sc);
	_screens[MULTI]->lastButton("map3", glm::vec4(435, 620, 25, 260));
	_screens[MULTI]->lastButton(START, SET, 3);

	_screens[MULTI]->addButton(glm::vec3(0, 7, 5), sc);
	_screens[MULTI]->lastButton("map4", glm::vec4(635, 815, 25, 260));
	_screens[MULTI]->lastButton(START, SET, 4);

	_screens[MULTI]->addButton(glm::vec3(-3, 7, 5), sc);
	_screens[MULTI]->lastButton("map5", glm::vec4(830, 1010, 25, 260));
	_screens[MULTI]->lastButton(START, SET, 5);	

	_screens[MULTI]->addButton(glm::vec3(-6, 7, 5), sc);
	_screens[MULTI]->lastButton("random", glm::vec4(1030, 1210, 25, 260));
	_screens[MULTI]->lastButton(START, SET, 6);

	_screens[MULTI]->addButton(glm::vec3(9, 7, -5), sc);
	_screens[MULTI]->lastButton("back", glm::vec4(75, 185, 680, 705));
	_screens[MULTI]->lastButton(PAGE, SET, MENU);

	addPict("option", OPTION, false);

	_screens[OPTION]->addButton(glm::vec3(-5, 7, 3), sc);
	_screens[OPTION]->lastButton("plus", glm::vec4(1010, 1080, 155, 180));
	_screens[OPTION]->lastButton(NB_BOX, ADD, 5);

	_screens[OPTION]->addButton(glm::vec3(-5, 7, 1), sc);
	_screens[OPTION]->lastButton("plus", glm::vec4(1010, 1080, 285, 310));
	_screens[OPTION]->lastButton(NB_IA, ADD, 1);

	_screens[OPTION]->addButton(glm::vec3(-5, 7, -1), sc);
	_screens[OPTION]->lastButton("plus", glm::vec4(1010, 1080, 415, 440));
	_screens[OPTION]->lastButton(BONUS, ADD, 5);

	_screens[OPTION]->addButton(glm::vec3(-5, 7, -3), sc);
	_screens[OPTION]->lastButton("plus", glm::vec4(1010, 1080, 550, 570));
	_screens[OPTION]->lastButton(WIDTH, ADD, 5);

	_screens[OPTION]->addButton(glm::vec3(-5, 7, -5), sc);
	_screens[OPTION]->lastButton("plus", glm::vec4(1010, 1080, 675, 700));
	_screens[OPTION]->lastButton(HEIGHT, ADD, 5);

	_screens[OPTION]->addButton(glm::vec3(-7, 7, 3), sc);
	_screens[OPTION]->lastButton("moins", glm::vec4(1140, 1215, 155, 180));
	_screens[OPTION]->lastButton(NB_BOX, ADD, -5);

	_screens[OPTION]->addButton(glm::vec3(-7, 7, 1), sc);
	_screens[OPTION]->lastButton("moins", glm::vec4(1140, 1215, 285, 310));
	_screens[OPTION]->lastButton(NB_IA, ADD, -1);

	_screens[OPTION]->addButton(glm::vec3(-7, 7, -1), sc);
	_screens[OPTION]->lastButton("moins", glm::vec4(1140, 1215, 415, 440));
	_screens[OPTION]->lastButton(BONUS, ADD, -5);

	_screens[OPTION]->addButton(glm::vec3(-7, 7, -3), sc);
	_screens[OPTION]->lastButton("moins", glm::vec4(1140, 1215, 550, 570));
	_screens[OPTION]->lastButton(WIDTH, ADD, -5);

	_screens[OPTION]->addButton(glm::vec3(-7, 7, -5), sc);
	_screens[OPTION]->lastButton("moins", glm::vec4(1140, 1215, 675, 700));
	_screens[OPTION]->lastButton(HEIGHT, ADD, -5);

	_screens[OPTION]->addButton(glm::vec3(9, 7, -5), sc);
	_screens[OPTION]->lastButton("back", glm::vec4(75, 190, 680, 705));
	_screens[OPTION]->lastButton(PAGE, SET, MENU);

	addPict("credits", CREDITS, false);

	_screens[CREDITS]->addButton(glm::vec3(9, 7, -5), sc);
	_screens[CREDITS]->lastButton("back", glm::vec4(75, 190, 680, 705));
	_screens[CREDITS]->lastButton(PAGE, SET, MENU);

	addPict("highscore", HIGHSCORE, false);
	_screens[HIGHSCORE]->addButton(glm::vec3(9, 7, -5), sc);
	_screens[HIGHSCORE]->lastButton("back", glm::vec4(75, 190, 680, 705));
	_screens[HIGHSCORE]->lastButton(PAGE, SET, MENU);

	addPict("sobre", LOADING, false);

	addPict("fond", THEMES, false);

	_screens[THEMES]->addButton(glm::vec3(9, 7, 5), sc);
	_screens[THEMES]->lastButton("ground", glm::vec4(75, 190, 25, 50));
	_screens[THEMES]->lastButton(NO_GROUND, SET, 0);

	_screens[THEMES]->addButton(glm::vec3(9, 7, 4), sc);
	_screens[THEMES]->lastButton("noground", glm::vec4(75, 190, 90, 115));
	_screens[THEMES]->lastButton(NO_GROUND, SET, 1);

	_screens[THEMES]->addButton(glm::vec3(3, 7, 5), sc);
	_screens[THEMES]->lastButton("themes_prev", glm::vec4(465, 580, 25, 50));
	_screens[THEMES]->lastButton(NM_THEME, ADD, -1);

	_screens[THEMES]->addButton(glm::vec3(0, 7, 5), sc);
	_screens[THEMES]->lastButton("themes_next", glm::vec4(666, 777, 25, 50));
	_screens[THEMES]->lastButton(NM_THEME, ADD, 1);

	_screens[THEMES]->addButton(glm::vec3(9, 7, -5), sc);
	_screens[THEMES]->lastButton("back", glm::vec4(75, 190, 680, 705));
	_screens[THEMES]->lastButton(PAGE, SET, MENU);
}
