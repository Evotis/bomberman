/*
 * file: main.cpp
 *
 * author: Thomas Nieto - nieto_t
 *
 * Tek_2 - project: bomberman
 */

#include <sstream>
#include <iostream>
#include <ctime>
#include <unistd.h>

#include "Systems.hpp"
#include "CPosition.hpp"
#include "AComponent.hpp"
#include "World.hpp"
#include "Save.hpp"
#include "SRender.hpp"
#include "SMenu.hpp"

std::map<eType, gdl::Texture> m_texture;

int main() {
  Menu engine(10, 10);
  engine.start();
  return 0;
}

