#include "AComponent.hpp"
#include "World.hpp"

void 			respawn(entity *player)
{
	t_pos		x;
	t_pos		y;

	x = (dynamic_cast<Component::Position *>((*player)[Component::POSITION]))->getStartX();
	y = (dynamic_cast<Component::Position *>((*player)[Component::POSITION]))->getStartY();

	(dynamic_cast<Component::Position *>((*player)[Component::POSITION]))->setPos(x, y);
	g_world->push(player);
}
