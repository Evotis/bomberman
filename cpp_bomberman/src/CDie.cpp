//
// CDie.cc for src in /home/n0t/dev/cpp/bomberman/cpp_bomberman/src
//
// Made by thomas nieto
// Login   <nieto_t@epitech.net>
//
// Started on  Sat May 10 19:06:55 2014 thomas nieto
// Last update Sat May 10 19:06:55 2014 thomas nieto
//

#include "CDie.hpp"


// ctors & dtors
Component::Die::Die() : Component::AComponent() {
  type_ = Component::DIE;
}

Component::Die::Die(const Die& oth) : Component::AComponent() {
  type_ = oth.type_;
}

Component::Die& Component::Die::operator=(const Die& lft) {
  if (this != &lft) {
    type_ = lft.type_;
  }

  return *this;
}

Component::Die::~Die() {}

// getters

