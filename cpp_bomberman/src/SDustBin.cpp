#include "World.hpp"

void dustbin()
{
  t_pos x;
  t_pos y;
  t_entlist entlist;
  t_entlistIt it;
  eType type;

  for (y = 0; y <= g_world->getY(); y++){
    for (x = 0; x <= g_world->getX(); x++)
    {
      entlist.clear();
      (*g_world)(x, y, entlist);

      for (it = entlist.begin(); it != entlist.end(); ++it)
      {
        type = static_cast<Component::Type *>((*(*it))[Component::TYPE])->geteType();
        if (((type == SPEED || type == RANGE || type == NBBOMB) && static_cast<Component::IsTaken *>((*(*it))[Component::ISTAKEN])->isItTaken() == true))
          deleteEntity(g_world->pop(*it));
        else if (type != WALL && type != SPEED && type != RANGE && type != NBBOMB && type != PLAYER && static_cast<Component::Alive *>((*(*it))[Component::ALIVE])->isAlive() == false)
          deleteEntity(g_world->pop(*it));
      }
    }
  }
}
