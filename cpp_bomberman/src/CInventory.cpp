#include "CInventory.hpp"

int changeSpeed;

Component::Inventory::Inventory(int range, int speed, int nbBomb)
{
	this->speed = speed;
	this->speed = 100;
	this->nbBomb = nbBomb;
	this->range = range;
	maxBomb = nbBomb;
	time_since_last_move = 0;
	changeSpeed = 2000;
}

Component::Inventory::~Inventory()
{}

Component::Inventory::Inventory(Inventory const &oth)
: Component::AComponent()
{
	speed = oth.getSpeed();
	nbBomb = oth.getNbBomb();
	range = oth.getRange();
	maxBomb = oth.getMaxBomb();
	changeSpeed = 2000;
}

Component::Inventory 		&Component::Inventory::operator=(Inventory const &oth)
{
	if (&oth != this)
	{
		speed = oth.getSpeed();
		nbBomb = oth.getNbBomb();
		range = oth.getRange();
		range = oth.getMaxBomb();
	}
	return *this;
}

int 			Component::Inventory::getRange() const
{
	return range;
}

int 			Component::Inventory::getSpeed() const
{
	return speed;
}

int 			Component::Inventory::getMaxBomb() const
{
	return maxBomb;
}

int 			Component::Inventory::getNbBomb() const
{
	return nbBomb;
}

void			Component::Inventory::setRange(int nRange)
{
	range = nRange;
}

void			Component::Inventory::decSpeed()
{
  if (changeSpeed < 3000)
    changeSpeed += 200;
}

void			Component::Inventory::setSpeed(int nSpeed)
{
    speed = nSpeed;
}

void			Component::Inventory::setNbBomb(int nNbBomb)
{
	nbBomb = nNbBomb;
	maxBomb = nNbBomb;
}

void			Component::Inventory::addRange()
{
	range += 1;
}

void 			Component::Inventory::addSpeed()
{
	speed += 20;
}

void 			Component::Inventory::addNbBomb()
{
	nbBomb += 1;
	maxBomb += 1;
}

void 			Component::Inventory::subNbBomb()
{
	nbBomb -= 1;
}

void 			Component::Inventory::addTime(float i)
{
  if (time_since_last_move <= speed)
    time_since_last_move += (i * changeSpeed);
}


void			Component::Inventory::reset()
{
	time_since_last_move = 0;
}

bool			Component::Inventory::canMove()
{
	if (time_since_last_move > speed)
			return (true);
	return (false);
}
