//
// Screen.cpp, eyzat_f
//

#include "RScreen.hpp"

Screen::Screen(int x, int y, int z, float sizeX, float sizeY, float sizeZ):
AObject(x, y, z, sizeX, sizeY, sizeZ), Sx(sizeX), Sy(sizeY), Sz(sizeZ)
{
  permute = false;
  marvin = NULL;
}

int  Screen::setScreen(std::string img)
{
  _rotation.x = 270;
  _rotation.z = 90;
  if (_texture.load(img) == false)
  {
    std::cerr << img << "fail texture" << std::endl;
    return (1);
  }
  return (0);
}

int  Screen::setScreen2(std::string img)
{
  permute = true;
  if (_texture2.load(img) == false)
  {
    std::cerr << img << "fail texture" << std::endl;
    return (1);
  }
  return (0);
}

bool range = false;
double tt = 0;

void Screen::update(gdl::Clock const &clock, gdl::Input &input)
{
  tt += clock.getElapsed();
  if (marvin != NULL && range == false && tt > 0.02)
  {
    tt = 0;
    marvin->setYRotation(270);
    marvin->translate(glm::vec3(-0.1, 0, 0));
    if (marvin->getPosition().x < -15)
      range = true;
  }
  if (marvin != NULL && range && tt > 0.02)
  {
    tt = 0;
    marvin->setYRotation(90);
    marvin->translate(glm::vec3(0.1, 0, 0));
    if (marvin->getPosition().x > 17)
      range = false;
  }
  for (unsigned int x = 0; x < xbutton.size(); ++x)
    xbutton[x]->update(clock, input);
}

void Screen::addButton(glm::vec3 pos, glm::vec3 scale)
{
  Button *nbutton = new Button(pos.x, pos.y, pos.z, scale.x, scale.y, scale.z);

  nbutton->initialize(1.0f);
  xbutton.push_back(nbutton);
}

void Screen::lastButton(std::string path, glm::vec4 width)
{
  xbutton.back()->setButton("./sprite/button/" + path + ".tga");
  xbutton.back()->setButton2("./sprite/button/" + path + "2.tga"); 
  xbutton.back()->setZone(width); 
}

void Screen::lastButton(eVal key, eOp op, int val)
{
  xbutton.back()->setEffect(key, op, val); 
}


void Screen::lastButton(int val)
{
  xbutton.back()->setKeyboard(val); 
}

void Screen::addMarvin(glm::vec3 pos)
{
  marvin = new Model(pos.x, pos.y, pos.z);
  marvin->initialize(0.0005f);
  marvin->stop(false);
  marvin->setPosition(pos.x, pos.y, pos.z, 270);
  // player1->setPosition(pos.x, pos.y, pos.z);
  // player1->setKey(SDLK_UP, SDLK_DOWN, SDLK_LEFT, SDLK_RIGHT);
  // xbutton.push_back(player1);
}


void Screen::choose_text()
{
  static int i;

  if (i < 10)
  {
    _texture.bind();
    i++;
  }
  else if (i < 20)
  {
    _texture2.bind();
    i++;
  }
  else
    i = 0;
}

void Screen::draw(gdl::AShader &shader, gdl::Clock const &clock)
{
  (void)clock;
  _texture.bind();
  _geometry.draw(shader, getTransformation(), GL_QUADS);
  for (size_t i = 0; i < xbutton.size(); ++i)
    xbutton[i]->draw(shader, clock);
  if (marvin != NULL)
    marvin->draw(shader, clock);
}

# define MAX(x) x / 2
# define MIN(x) (x / 2) * (-1)

bool Screen::initialize(float speed)
{
  _speed = speed;
  _geometry.pushVertex(glm::vec3(MAX(Sx), MIN(Sy), MIN(Sz)));
  _geometry.pushVertex(glm::vec3(MAX(Sx), MAX(Sy), MIN(Sz)));
  _geometry.pushVertex(glm::vec3(MAX(Sx), MAX(Sy), MAX(Sz)));
  _geometry.pushVertex(glm::vec3(MAX(Sx), MIN(Sy), MAX(Sz)));
  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));
  _geometry.build();
  return (true);
}
