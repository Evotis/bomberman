
#include "RCube.hpp"

extern std::map<eType, gdl::Texture> m_texture;

Cube::Cube(int x, int y, int z, eType type):
AObject(x, y, z, 1, 1, 1), Sx(1), Sy(1), Sz(1)
{
  _type = type;
}

void Cube::update(gdl::Clock const &, gdl::Input &)
{
}

void Cube::draw(gdl::AShader &shader, gdl::Clock const &)
{
  m_texture[_type].bind();
  _geometry.draw(shader, getTransformation(), GL_QUADS);
}

# define MAX(x) x / 2
# define MIN(x) (x / 2) * (-1)

bool Cube::initialize(float speed)
{
  _speed = speed;

  // _geometry.setColor(glm::vec4(0, 1, 1, 1)); // dark green mode
  _geometry.pushVertex(glm::vec3(MAX(Sx), MIN(Sy), MIN(Sz))); //bas droite
  _geometry.pushVertex(glm::vec3(MAX(Sx), MAX(Sy), MIN(Sz))); //haut droite
  _geometry.pushVertex(glm::vec3(MIN(Sx), MAX(Sy), MIN(Sz))); //haut gauche
  _geometry.pushVertex(glm::vec3(MIN(Sx), MIN(Sy), MIN(Sz))); //bas gauche

  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));

  _geometry.pushVertex(glm::vec3(MAX(Sx), MIN(Sy), MIN(Sz)));
  _geometry.pushVertex(glm::vec3(MAX(Sx), MAX(Sy), MIN(Sz)));
  _geometry.pushVertex(glm::vec3(MAX(Sx), MAX(Sy), MAX(Sz)));
  _geometry.pushVertex(glm::vec3(MAX(Sx), MIN(Sy), MAX(Sz)));

  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));

  _geometry.pushVertex(glm::vec3(MIN(Sx), MIN(Sy), MAX(Sz)));
  _geometry.pushVertex(glm::vec3(MIN(Sx), MAX(Sy), MAX(Sz)));
  _geometry.pushVertex(glm::vec3(MIN(Sx), MAX(Sy), MIN(Sz)));
  _geometry.pushVertex(glm::vec3(MIN(Sx), MIN(Sy), MIN(Sz)));

  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));

  _geometry.pushVertex(glm::vec3(MAX(Sx), MAX(Sy), MAX(Sz)));
  _geometry.pushVertex(glm::vec3(MAX(Sx), MAX(Sy), MIN(Sz)));
  _geometry.pushVertex(glm::vec3(MIN(Sx), MAX(Sy), MIN(Sz)));
  _geometry.pushVertex(glm::vec3(MIN(Sx), MAX(Sy), MAX(Sz)));

  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));

  _geometry.pushVertex(glm::vec3(MAX(Sx), MIN(Sy), MAX(Sz)));
  _geometry.pushVertex(glm::vec3(MAX(Sx), MAX(Sy), MAX(Sz)));
  _geometry.pushVertex(glm::vec3(MIN(Sx), MAX(Sy), MAX(Sz)));
  _geometry.pushVertex(glm::vec3(MIN(Sx), MIN(Sy), MAX(Sz)));
  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));

  _geometry.build();
  return (true);
}
