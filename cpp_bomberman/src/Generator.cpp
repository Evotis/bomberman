//
// generator.cpp for src in /home/n0t/dev/cpp/bomberman/cpp_bomberman/src
//
// Made by thomas nieto
// Login   <nieto_t@epitech.net>
//
// Started on  Tue Jun 10 20:19:58 2014 thomas nieto
// Last update Tue Jun 10 20:19:58 2014 thomas nieto
//

#include "Generator.hpp"

inline static density down(density nb) {
  return nb > 1 ? nb - floor(nb) : nb;
}

inline static t_pos pair(t_pos nb) {
  return nb % 2;
}

// ctors & dtors
Generator::Generator(t_pos x, t_pos y, density box, density ia, density bonus)
: x_(x), y_(y), begin_(1, 1), end_(x_ - 1, y_ - 1), surface_((x * y) - 6),
box_(down(box)), ia_(down(ia)), bonus_(down(bonus)), pushed_(surface_, std::make_pair(empty, empty)) {
  srand(time(NULL));

  if (end_.first + 1 <= begin_.first || end_.second  + 1 <= begin_.second)
    throw InvalidCoordinates();

   placeWall();

  if (box_)
    placeBox();

  if (ia_)
    placeIa();
}

Generator::~Generator() {}


bool Generator::pushed(const std::pair<t_pos, t_pos>& toPut, t_pos limit) const {
  t_pos current = 0;
  t_savePos::const_iterator it;

  if ((toPut.first <= begin_.first && toPut.second <= begin_.second)
    || (toPut.first >= end_.first && toPut.second  >= end_.second))
    return true;


  for (it = pushed_.begin(); current < limit; ++it, ++current) {
    if (toPut.first == it->first && toPut.second == it->second)
      return true;
  }

  return false;
}

void Generator::createBonus(char nb, const std::pair<t_pos, t_pos>& pos) {
  if (!nb)
    init_.push_back(createSpeed(pos.first, pos.second));
  else if (nb == 1)
    init_.push_back(createRange(pos.first, pos.second));
  else
    init_.push_back(createNbBomb(pos.first, pos.second));
}

t_pos Generator::checkProc(density dens) const {
  t_pos ret = surface_ * dens;

  if (ret > 10000)
    ret = surface_ * 0.1;

  return ret;
}

// actions
void Generator::placeWall() {
  t_pos x = 1;
  t_pos y = 1;
  short i = 0;


  while (x < end_.first && y < end_.second) {
    init_.push_back(createWall(x, y));
    pushed_[i] = std::make_pair(x, y);
    y += 2;

    if (y >= y_ - 1) {
      y = 1;
      x += 2;
    }

    i += 1;
    surface_ -= 1;
  }
}

// uncomment when bonus are ready
void Generator::placeBox() {
  t_pos x = 0;
  t_pos i = init_.size();
  t_pos aim = checkProc(box_);
  std::pair<t_pos, t_pos> toPut;
  char bonus;

  while (aim) {
    toPut = std::make_pair(x, rand() % y_);

    if (!pushed(toPut, i)) {

      if (rand() % 10 < bonus_ * 10) {
        bonus = rand() % 3;
        createBonus(bonus, toPut);
      }

      init_.push_back(createBox(toPut.first, toPut.second));
      pushed_[i] = toPut;
      i += 1;
      aim -= 1;
      surface_ -= 1;
    }

  x += 1;

  if (x > x_ - 1)
      x = 0;
  }
}

// uncomment when ia are ready
void Generator::placeIa() {
  t_pos x = 0;
  t_pos i = init_.size();
  t_pos aim = surface_ - ia_ > 0 ? ia_ : 0;
  std::pair<t_pos, t_pos> toPut;

  while (aim) {
    toPut = std::make_pair(x, rand() % y_);

    if (!pushed(toPut, i)) {
      init_.push_back(createIa(toPut.first, toPut.second));
      pushed_[i] = toPut;
      i += 1;
      aim -= 1;
      surface_ -= 1;
    }

  x += 1;

  if (x > x_ - 1)
      x = 0;
  }
}

// getters
const std::list<entity *>& Generator::get() const {
  return init_;
}

t_pos Generator::getX() const {
  return x_;
}

t_pos Generator::getY() const {
  return y_;
}

const t_defend& Generator::getBegin() const {
  return begin_;
}

const t_defend& Generator::getEnd() const {
  return end_;
}


t_pos Generator::getSurface() const {
  return surface_;
}

density Generator::getBox() const {
  return box_;
}

density Generator::getIa() const {
  return ia_;
}

density Generator::getBonus() const {
  return bonus_;
}

//dump
std::ostream& operator<<(std::ostream& lft, const Generator& rht) {
  std::list<entity *> lst = rht.get();
  std::list<entity *>::iterator it;

  lft << "!!!! Surface is decrementing for each gameobject put on map !!!" << std::endl
      << "   Generator dump:" << std::endl
      << "--------------------" << std::endl
      << "      x: " << rht.getX() << std::endl
      << "      y: " << rht.getY() << std::endl
      << "  begin: " << (rht.getBegin()).first << ", " << (rht.getBegin()).second << std::endl
      << "    end: " << (rht.getEnd()).first << ", " << (rht.getEnd()).second << std::endl
      << "surface: " << rht.getSurface() << std::endl
      << "    box: " << rht.getBox() << std::endl
      << "     ia: " << rht.getIa() << std::endl
      << "  bonus: " << rht.getBonus() << std::endl
      << " pushed: " << rht.get().size() << std::endl
      << "      ---" << std::endl
      << "    map: " << std::endl;

  for (it = lst.begin(); it != lst.end(); ++it) {
    lft << "\t coord: " << static_cast<Component::Position *>((*(*it))[Component::POSITION])->getX()
        << ", " << static_cast<Component::Position *>((*(*it))[Component::POSITION])->getY()
        << " - type: " << static_cast<Component::Type *>((*(*it))[Component::TYPE])->geteType()
        << std::endl;
  }

  lft << std::endl;
  return lft;
}
