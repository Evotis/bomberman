//
// Button.cpp, eyzat_f
//

#include "RButton.hpp"


extern std::map<eVal, int> g_config;


Button::Button(int x, int y, int z, float sizeX, float sizeY, float sizeZ):
AObject(x, y, z, sizeX, sizeY, sizeZ), Sx(sizeX), Sy(sizeY), Sz(sizeZ)
{
  permute = false;
  clicked = false;
  _k = 0;
}

int  Button::setButton(std::string img)
{
  _rotation.x = 270;
  _rotation.z = 90;
  if (_texture.load(img) == false)
  {
    std::cerr << "fail texture" << std::endl;
    return (1);
  }
  return (0);
}

int  Button::setButton2(std::string img)
{
  permute = false;
  if (_texture2.load(img) == false)
  {
    std::cerr << "fail texture" << std::endl;
    return (1);
  }
  return (0);
}

void   Button::setZone(glm::vec4 zone_clikable)
{
  dx = zone_clikable.x;
  fx = zone_clikable.y;
  dy = zone_clikable.z;
  fy = zone_clikable.w;
}

void   Button::setEffect(eVal key, eOp op, int val)
{
  _key = key;
  _op  = op;
  _val = val;
}

void  Button::setKeyboard(int k)
{
  _k   = k;
}


void   Button::effect()
{
  if (_op == SET)
    g_config[_key] = _val;
  else if (_op == ADD)
    g_config[_key] += _val;
}

void Button::update(gdl::Clock const &clock, gdl::Input &input)
{
  int sx;
  int sy;

  (void)clock;
  sx = input.getMousePosition().x;
  sy = input.getMousePosition().y;
  if (sx >= dx && sx <= fx && sy >= dy && sy <= fy)
  {
    permute = true;
    if (input.getKey(SDL_BUTTON_LEFT))
      clicked = true;
    else if (clicked)
    {
      clicked = false;
      effect();
    }
  } 
  else
    permute = false;
  if (_k != 0 && input.getKey(_k))
    effect();
}

void Button::choose_text()
{
  static int i;

  if (i < 50)
  {
    _texture.bind();
    i++;
  }
  else if (i < 100)
  {
    _texture2.bind();
    i++;
  }
  else
    i = 0;
}

void Button::toggle()
{
  permute = !permute;
}

void Button::draw(gdl::AShader &shader, gdl::Clock const &clock)
{
  (void)clock;
  if (permute)
    _texture2.bind();
  else
    _texture.bind();
  _geometry.draw(shader, getTransformation(), GL_QUADS);
}

# define MAX(x) x / 2
# define MIN(x) (x / 2) * (-1)

bool Button::initialize(float speed)
{
  _speed = speed;
  _geometry.pushVertex(glm::vec3(MAX(Sx), MIN(Sy), MIN(Sz)));
  _geometry.pushVertex(glm::vec3(MAX(Sx), MAX(Sy), MIN(Sz)));
  _geometry.pushVertex(glm::vec3(MAX(Sx), MAX(Sy), MAX(Sz)));
  _geometry.pushVertex(glm::vec3(MAX(Sx), MIN(Sy), MAX(Sz)));
  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));
  _geometry.build();
  return (true);
}
