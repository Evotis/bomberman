//
// Back.cpp, eyzat_f
//

#include "RSkyBox.hpp"

gdl::Texture _textureB;
int	back = 0;

SkyBox::SkyBox(int x, int y, int z, float sizeX, float sizeY, float sizeZ, std::string path):
  AObject(x, y, z, sizeX, sizeY, sizeZ)
{
  if (_textureB.load("./" + path + "/fond.tga") == false)
    std::cerr << "fail texture back" << std::endl;
}

void SkyBox::update(gdl::Clock const &clock, gdl::Input &input)
{
  (void)clock;
  (void)input;
}

void SkyBox::draw(gdl::AShader &shader, gdl::Clock const &clock)
{
  (void)clock;
  _textureB.bind();
  _geometry.draw(shader, getTransformation(), GL_QUADS);
}

void SkyBox::draw(gdl::AShader &shader, int x, int y)
{
  setPosition(x, 0, y, 0);
  _textureB.bind();
  _geometry.draw(shader, getTransformation(), GL_QUADS);
}

bool SkyBox::initialize(float speed, int sX, int sY)
{
  _speed = speed;


  sX = 30;
  sY = 30;
  glm::vec3	hdar(-sX - 5, 10, sY + 10);
  glm::vec3	hgar(sX + 10, 10, sY + 10);

  glm::vec3	bdar(-sX - 5, -50, sY + 10);
  glm::vec3	bgar(sX + 10, -50, sY + 10);

  glm::vec3	bgav(sX + 10, -50, -sY - 10);
  glm::vec3	hgav(sX + 10, 10, -sY - 10);

  glm::vec3	bdav(-sX - 5, -50, -sY - 10);
  glm::vec3	hdav(-sX - 5, 10, -sY - 10);






  _geometry.pushVertex(hgar);
  _geometry.pushVertex(bgar);
  _geometry.pushVertex(bgav);
  _geometry.pushVertex(hgav);

  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));




  _geometry.pushVertex(hdar);
  _geometry.pushVertex(bdar);
  _geometry.pushVertex(bdav);
  _geometry.pushVertex(hdav);

  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));




  _geometry.pushVertex(bdar);
  _geometry.pushVertex(bgar);
  _geometry.pushVertex(bgav);
  _geometry.pushVertex(bdav);

  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));


  _geometry.pushVertex(bgav);
  _geometry.pushVertex(bdav);
  _geometry.pushVertex(hdav);
  _geometry.pushVertex(hgav);

  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));

  // Tres important, on n'oublie pas de build la geometrie pour envoyer ses informaitions aux GPU
  _geometry.build();
  // setPosition(0, 0, -10);
  return (true);
}
