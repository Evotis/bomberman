/**
 * file: Entity.cpp
 *
 * author: Thomas Nieto - nieto_t
 *
 * Tek_2 - project: cpp_bomberman
 */

#include "Entity.hpp"

static void deleteComponent(std::pair<const Component::type_t, Component::AComponent *>& comp) {
  if (comp.second)
    delete comp.second;
}

void deleteEntity(entity *ent) {
  std::for_each(ent->begin(), ent->end(), deleteComponent);
  delete ent;
}

entity *createBomb(t_pos x, t_pos y) {
  entity *bomb = new entity();

  (*bomb)[Component::DIE] = new Component::Die();
  (*bomb)[Component::POSITION] = new Component::Position(x, y);
  (*bomb)[Component::DESTROYABLE] = new Component::Destroyable();
  (*bomb)[Component::SOUND] = new Component::Sound();
  (*bomb)[Component::TYPE] = new Component::Type(BOMB);
  (*bomb)[Component::ALIVE] = new Component::Alive();
  (*bomb)[Component::INVENTORY] = new Component::Inventory();
  return bomb;
}

entity *createSpeed(t_pos x, t_pos y) {
  entity *bonus = new entity();

  (*bonus)[Component::POSITION] = new Component::Position(x, y);
  (*bonus)[Component::SOUND] = new Component::Sound();
  (*bonus)[Component::TYPE] = new Component::Type(SPEED);
  (*bonus)[Component::ALIVE] = new Component::Alive(false);
  (*bonus)[Component::ISTAKEN] = new Component::IsTaken();
  return bonus;
}

entity *createRange(t_pos x, t_pos y) {
  entity *bonus = new entity();

  (*bonus)[Component::POSITION] = new Component::Position(x, y);
  (*bonus)[Component::SOUND] = new Component::Sound();
  (*bonus)[Component::TYPE] = new Component::Type(RANGE);
  (*bonus)[Component::ALIVE] = new Component::Alive(false);
  (*bonus)[Component::ISTAKEN] = new Component::IsTaken();
  return bonus;
}

entity *createNbBomb(t_pos x, t_pos y) {
  entity *bonus = new entity();

  (*bonus)[Component::POSITION] = new Component::Position(x, y);
  (*bonus)[Component::SOUND] = new Component::Sound();
  (*bonus)[Component::TYPE] = new Component::Type(NBBOMB);
  (*bonus)[Component::ALIVE] = new Component::Alive(false);
  (*bonus)[Component::ISTAKEN] = new Component::IsTaken();
  return bonus;
}

entity *createBox(t_pos x, t_pos y) {
  entity *box = new entity();

  (*box)[Component::DIE] = new Component::Die();
  (*box)[Component::POSITION] = new Component::Position(x, y);
  (*box)[Component::DESTROYABLE] = new Component::Destroyable();
  (*box)[Component::SOUND] = new Component::Sound();
  (*box)[Component::TYPE] = new Component::Type(BOX);
  (*box)[Component::ALIVE] = new Component::Alive();
  return box;
}

entity *createIa(t_pos x, t_pos y) {
  entity *ia = new entity();

  (*ia)[Component::DIE] = new Component::Die();
  (*ia)[Component::SCRIPT] = new Component::Script();
  (*ia)[Component::POSITION] = new Component::Position(x, y);
  (*ia)[Component::INVENTORY] = new Component::Inventory();
  (*ia)[Component::DESTROYABLE] = new Component::Destroyable();
  (*ia)[Component::SCORE] = new Component::Score();
  (*ia)[Component::LIFE] = new Component::Life();
  (*ia)[Component::SOUND] = new Component::Sound();
  (*ia)[Component::GEOMETRY] = new Component::Geometry(x, y, IA);
  (*ia)[Component::TYPE] = new Component::Type(IA);
  (*ia)[Component::ALIVE] = new Component::Alive();
  return ia;
}

entity *createPlayer(t_pos x, t_pos y) {
  entity *player = new entity();

  (*player)[Component::DIE] = new Component::Die();
  (*player)[Component::PHYSIC] = new Component::Physic();
  (*player)[Component::POSITION] = new Component::Position(x, y);
  (*player)[Component::INVENTORY] = new Component::Inventory();
  (*player)[Component::DESTROYABLE] = new Component::Destroyable();
  (*player)[Component::SCORE] = new Component::Score();
  (*player)[Component::LIFE] = new Component::Life();
  (*player)[Component::SOUND] = new Component::Sound();
  (*player)[Component::GEOMETRY] = new Component::Geometry(x, y, PLAYER);
  (*player)[Component::TYPE] = new Component::Type(PLAYER);
  (*player)[Component::ALIVE] = new Component::Alive();
  (*player)[Component::SCREEN] = new Component::Screen();
  (*player)[Component::KEY] = new Component::Key();
  return player;
}

entity *createWall(t_pos x, t_pos y) {
  entity *wall = new entity();

  (*wall)[Component::POSITION] = new Component::Position(x, y);
  (*wall)[Component::TYPE] = new Component::Type(WALL);
  (*wall)[Component::ALIVE] = new Component::Alive();
  return wall;
}

entity        *createDeflag(t_pos x, t_pos y)
{
  entity      *deflag = new entity();

  (*deflag)[Component::POSITION] = new Component::Position(x, y);
  (*deflag)[Component::TYPE] = new Component::Type(DEFLAGRATION);
  (*deflag)[Component::ALIVE] = new Component::Alive();
  return deflag;
}
