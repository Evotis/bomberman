//
// CExplode.cc for src in /home/n0t/dev/cpp/bomberman/cpp_bomberman/src
//
// Made by thomas nieto
// Login   <nieto_t@epitech.net>
//
// Started on  Sat May 10 19:06:55 2014 thomas nieto
// Last update Sat May 10 19:06:55 2014 thomas nieto
//

#include "CDestroyable.hpp"


// ctors & dtors
Component::Destroyable::Destroyable() : Component::AComponent() {
  type_ = Component::DESTROYABLE;
}

Component::Destroyable::Destroyable(const Destroyable& oth) : Component::AComponent() {
  type_ = oth.type_;
}

Component::Destroyable& Component::Destroyable::operator=(const Destroyable& lft) {
  if (this != &lft) {
    type_ = lft.type_;
  }

  return *this;
}

Component::Destroyable::~Destroyable() {}

// getters

