//

// AObject.cpp, eyzat_f
//

#include "AObject.hpp"

AObject::AObject(int x, int y, int z, float, float, float)
  : _position(x, y, z), _rotation(0, 0, 0), _scale(1, 1, 1)
{
}

AObject::~AObject()
{
  
}

void	AObject::translate(glm::vec3 const &v)
{
  _position += v;
}

void	AObject::rotate(glm::vec3 const & axis, float angle)
{
  _position += axis * angle;
}

void	AObject::scale(glm::vec3 const &scale)
{
  _scale *= scale;
}

void	AObject::setPosition(int x, int y, int z, int rotation)
{
  glm::vec3 position(x, y, z);

  _position = position;
  _rotation.y = rotation;
}

void  AObject::setYRotation(int y)
{
  _rotation.y = y;
}

glm::vec3	AObject::getPosition()
{
  return (_position);
}

glm::mat4	AObject::getTransformation()
{
  glm::mat4 transform(1);

  transform = glm::rotate(transform, _rotation.x, glm::vec3(1, 0, 0));
  transform = glm::translate(transform, _position);
  transform = glm::rotate(transform, _rotation.y, glm::vec3(0, 1, 0));
  transform = glm::rotate(transform, _rotation.z, glm::vec3(0, 0, 1));
  transform = glm::scale(transform, _scale);
  return (transform);
}

void  AObject::update(gdl::Clock const &, gdl::Input&)
{
}
