#include "CIsTaken.hpp"

Component::IsTaken::IsTaken()
{
	taken = false;
}

Component::IsTaken::~IsTaken()
{}

Component::IsTaken::IsTaken(IsTaken const &oth)
	: Component::AComponent()
{
	taken = oth.isItTaken();
}

Component::IsTaken 	&Component::IsTaken::operator=(IsTaken const &oth)
{
	if (this != &oth)
	{
		taken = oth.isItTaken();		
	}
	return *this;
}

bool		Component::IsTaken::isItTaken() const
{
	return taken;
}

void 		Component::IsTaken::takeIt()
{
	taken = true;
}