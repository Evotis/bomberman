//
// CGeometry.cc for src in /home/n0t/dev/cpp/bomberman/cpp_bomberman/src
//
// Made by thomas nieto
// Login   <nieto_t@epitech.net>
//
// Started on  Sat May 10 19:06:55 2014 thomas nieto
// Last update Sat May 10 19:06:55 2014 thomas nieto
//

#include "CGeometry.hpp"

// ctors & dtors
Component::Geometry::Geometry(int x, int y, eType type) : Component::AComponent() {
  type_ = Component::GEOMETRY;
  if (type == PLAYER || type == IA)
    cube = new Model(x, 0, y);
  //else
  //cube = new Cube(x, 0, y, type);
  cube->initialize(10.0f);
}

Component::Geometry::Geometry(const Geometry& oth) : Component::AComponent() {
  type_ = oth.type_;
}

Component::Geometry& Component::Geometry::operator=(const Geometry& lft) {
  if (this != &lft) {
    type_ = lft.type_;
  }

  return *this;
}

Component::Geometry::~Geometry() {}

// getters

void	Component::Geometry::pos(glm::vec3 & pos, int rotation)
{
	cube->setPosition(pos.x, pos.y, pos.z, rotation);
}

void  Component::Geometry::stop(bool state)
{
  cube->stop(state);
}

void  Component::Geometry::update(gdl::Clock const &clock, gdl::Input &input)
{
  cube->update(clock, input);
}

void     Component::Geometry::draw(gdl::AShader &shader, gdl::Clock const &clock)
{
  cube->draw(shader, clock);
}