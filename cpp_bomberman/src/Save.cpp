#include <fstream>
#include <sstream>
#include "World.hpp"
#include "Save.hpp"
#include "SaveError.hpp"

bool                    save(std::string const &file)
{
	std::ofstream       flux(file.c_str());
	t_pos               x;
	t_pos               y;
	t_entlist           entlist;
	t_entlistIt         it;
	int                 flag = 0;

	try
	{
		if (!flux)
			throw SaveError("Wrong file name");
	}
	catch (std::exception &e)
	{
		std::cout << e.what() << std::endl;
		return false;
	}

	flux << g_world->getX() << " " << g_world->getY() << std::endl;
	for (y = 0; y < g_world->getY(); ++y)
	{
		for (x = 0; x < g_world->getX(); ++x)
		{
			entlist.clear();
			(*g_world)(x, y, entlist);
			for (it = entlist.begin(); it != entlist.end(); ++it)
			{
				flag = 1;
				eType type = static_cast<Component::Type *>((*(*it))[Component::TYPE])->geteType();
				if (type != DEFLAGRATION && type != BOMB)
				{
					flux << type << " " << x << " " << y;
					if (type == PLAYER || type == IA)
					{
						int range = static_cast<Component::Inventory *>((*(*it))[Component::INVENTORY])->getRange();
						int speed = static_cast<Component::Inventory *>((*(*it))[Component::INVENTORY])->getSpeed();
						int nbBomb = static_cast<Component::Inventory *>((*(*it))[Component::INVENTORY])->getMaxBomb();
						int score = static_cast<Component::Score *>((*(*it))[Component::SCORE])->getScore();
						int life = static_cast<Component::Life *>((*(*it))[Component::LIFE])->getLife();
						flux << " " << range << " " << speed << " " << nbBomb << " " << score << " " << life;
					}
				}
				if (flag == 1)
				{
					flux << std::endl;
				}
				flag = 0;
			}
		}
	}
	return true;
}

static int             cut(std::string line, int x, int y)
{
	std::string         ret;
	int                 value;

	while (x <= y)
	{
		ret += line[x];
		++x;
	}

	std::istringstream buffer(ret);
	buffer >> value;
	return (value);
}

static void                    extractWall(std::string &line)
{
	size_t              after = 0;
	size_t              before = 0;
	int                 value[6];
	entity              *ent;
	int                 idx = 0;

	while ((after = line.find(" ", after)) != std::string::npos)
	{
		if (idx > 2)
			throw SaveError("Save file is corrupted");
		value[idx] = cut(line, before, after);
		after += 1;
		before = after;
		idx++;
	}
	value[idx] = cut(line, before, line.length());
	ent = createWall(value[1], value[2]);
	g_world->push(ent);
}

static void                    extractPlayer(std::string &line)
{
	size_t              after = 0;
	size_t              before = 0;
	int                 value[8];
	entity              *ent;
	int                 idx = 0;

	while ((after = line.find(" ", after)) != std::string::npos)
	{
		if (idx > 7)
			throw SaveError("Save file is corrupted");
		value[idx] = cut(line, before, after);
		after += 1;
		before = after;
		idx++;
	}
	value[idx] = cut(line, before, line.length());
	ent = createPlayer(value[1], value[2]);
	(static_cast<Component::Inventory *>((*ent)[Component::INVENTORY]))->setRange(value[3]);
	(static_cast<Component::Inventory *>((*ent)[Component::INVENTORY]))->setSpeed(value[4]);
	(static_cast<Component::Inventory *>((*ent)[Component::INVENTORY]))->setNbBomb(value[5]);
	(static_cast<Component::Score *>((*ent)[Component::SCORE]))->setScore(value[6]);
	(static_cast<Component::Life *>((*ent)[Component::LIFE]))->setLife(value[7]);
	g_world->push(ent);
}

static void                    extractIa(std::string &line)
{
	size_t              after = 0;
	size_t              before = 0;
	int                 value[8];
	entity              *ent;
	int                 idx = 0;

	while ((after = line.find(" ", after)) != std::string::npos)
	{
		if (idx > 7)
			throw SaveError("Save file is corrupted");
		value[idx] = cut(line, before, after);
		after += 1;
		before = after;
		idx++;
	}
	value[idx] = cut(line, before, line.length());
	ent = createIa(value[1], value[2]);
	(static_cast<Component::Inventory *>((*ent)[Component::INVENTORY]))->setRange(value[3]);
	(static_cast<Component::Inventory *>((*ent)[Component::INVENTORY]))->setSpeed(value[4]);
	(static_cast<Component::Inventory *>((*ent)[Component::INVENTORY]))->setNbBomb(value[5]);
	(static_cast<Component::Score *>((*ent)[Component::SCORE]))->setScore(value[6]);
	(static_cast<Component::Life *>((*ent)[Component::LIFE]))->setLife(value[7]);
	g_world->push(ent);
}

static void                    extractBox(std::string &line)
{
	size_t              after = 0;
	size_t              before = 0;
	int                 value[6];
	entity              *ent;
	int                 idx = 0;

	while ((after = line.find(" ", after)) != std::string::npos)
	{
		if (idx > 2)
			throw SaveError("Save file is corrupted");
		value[idx] = cut(line, before, after);
		after += 1;
		before = after;
		idx++;
	}
	value[idx] = cut(line, before, line.length());
	ent = createBox(value[1], value[2]);
	g_world->push(ent);
}

static void                    extractSpeed(std::string &line)
{
	size_t              after = 0;
	size_t              before = 0;
	int                 value[6];
	entity              *ent;
	int                 idx = 0;

	while ((after = line.find(" ", after)) != std::string::npos)
	{
		if (idx > 2)
			throw SaveError("Save file is corrupted");
		value[idx] = cut(line, before, after);
		after += 1;
		before = after;
		idx++;
	}
	value[idx] = cut(line, before, line.length());
	ent = createSpeed(value[1], value[2]);
	g_world->push(ent);
}

static void                    extractRange(std::string &line)
{
	size_t              after = 0;
	size_t              before = 0;
	int                 value[6];
	entity              *ent;
	int                 idx = 0;

	while ((after = line.find(" ", after)) != std::string::npos)
	{
		if (idx > 2)
			throw SaveError("Save file is corrupted");
		value[idx] = cut(line, before, after);
		after += 1;
		before = after;
		idx++;
	}
	value[idx] = cut(line, before, line.length());
	ent = createRange(value[1], value[2]);
	g_world->push(ent);
}

static void                    extractNbBomb(std::string &line)
{
	size_t              after = 0;
	size_t              before = 0;
	int                 value[6];
	entity              *ent;
	int                 idx = 0;

	while ((after = line.find(" ", after)) != std::string::npos)
	{
		if (idx > 2)
			throw SaveError("Save file is corrupted");
		value[idx] = cut(line, before, after);
		after += 1;
		before = after;
		idx++;
	}
	value[idx] = cut(line, before, line.length());
	ent = createNbBomb(value[1], value[2]);
	g_world->push(ent);
}

static void	 			mapSize(std::string const &line)
{
	int 			x = 0;
	int 			y = 0;
	int 			_x = 0;

	x = line.find(" ", x);
	_x = cut(line, 0, x);
	y = cut(line, x, line.length());
	g_world = new World(_x, y);
}

bool                                                  load(std::string file)
{
	std::ifstream                                     ffile(file.c_str());
	std::string                                       line;
	std::map<char, void (*)(std::string &)>           ptr;
	std::map<char, void (*)(std::string &)>::iterator it;
	int 											  idx = 0;

	try
	{
		if (!ffile.is_open())
			throw SaveError("Wrong file name");
	}
	catch (std::exception const &e)
	{
		std::cout << e.what() << std::endl;
		return false;
	}
	ptr['0'] = &extractWall;
	ptr['1'] = &extractPlayer;
	ptr['2'] = &extractIa;
	ptr['3'] = &extractBox;
	ptr['6'] = &extractSpeed;
	ptr['7'] = &extractRange;
	ptr['8'] = &extractNbBomb;
	getline(ffile, line);
	mapSize(line);
	while (getline(ffile, line))
	{
		try
		{

			if ((it = ptr.find(line[0])) != ptr.end())
            {
                idx++;
                it->second(line);
            }
        }
        catch (std::exception const &e)
        {
            std::cout << e.what() << std::endl;
            return false;
        }
    }
    if (idx < 1)
        return false;
    return true;
}
