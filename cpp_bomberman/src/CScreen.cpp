//
// CScreen.cc for src in /home/n0t/dev/cpp/bomberman/cpp_bomberman/src
//
// Made by thomas nieto
// Login   <nieto_t@epitech.net>
//
// Started on  Sat May 10 19:06:55 2014 thomas nieto
// Last update Sat May 10 19:06:55 2014 thomas nieto
//

#include "CScreen.hpp"


// ctors & dtors
Component::Screen::Screen() : Component::AComponent() {
  type_ = Component::SCREEN;
}

Component::Screen::Screen(const Screen& oth) : Component::AComponent() {
  type_ = oth.type_;
}

Component::Screen& Component::Screen::operator=(const Screen& lft) {
  if (this != &lft) {
    type_ = lft.type_;
  }

  return *this;
}

Component::Screen::~Screen() {}

// getters

void Component::Screen::window()
{
	glViewport(posx, posy, width, height);
}

void Component::Screen::setScreen(glm::vec4 & data)
{
	posx = data.x;
	posy = data.y;
	width = data.z;
	height = data.w;
}
