//
// CScript.cc for src in /home/n0t/dev/cpp/bomberman/cpp_bomberman/src
//
// Made by thomas nieto
// Login   <nieto_t@epitech.net>
//
// Started on  Sat May 10 19:06:55 2014 thomas nieto
// Last update Sat May 10 19:06:55 2014 thomas nieto
//

#include "CScript.hpp"


// ctors & dtors
Component::Script::Script() : Component::AComponent() {
  type_ = Component::SCRIPT;
}

Component::Script::Script(const Script& oth) : Component::AComponent() {
  type_ = oth.type_;
}

Component::Script& Component::Script::operator=(const Script& lft) {
  if (this != &lft) {
    type_ = lft.type_;
  }

  return *this;
}

Component::Script::~Script() {}

// getters

