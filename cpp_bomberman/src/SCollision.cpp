#include "SCollision.hpp"

Collision g_collision;

Collision::Collision()
{
	instr[PLAYER][WALL] = &Collision::doNothing;
	instr[PLAYER][PLAYER] = &Collision::goAhead;
	instr[PLAYER][IA] = &Collision::goAhead;
	instr[PLAYER][BOX] = &Collision::doNothing;
	instr[PLAYER][DEFLAGRATION] = &Collision::dieLeft;
	instr[PLAYER][BOMB] = &Collision::doNothing;
	instr[PLAYER][SPEED] = &Collision::onBonus;
	instr[PLAYER][RANGE] = &Collision::onBonus;
	instr[PLAYER][NBBOMB] = &Collision::onBonus;
	instr[PLAYER][NOTHING] = &Collision::goAhead;

	instr[IA][WALL] = &Collision::doNothing;
	instr[IA][PLAYER] = &Collision::goAhead;
	instr[IA][IA] = &Collision::goAhead;
	instr[IA][BOX] = &Collision::doNothing;
	instr[IA][DEFLAGRATION] = &Collision::dieLeft;
	instr[IA][BOMB] = &Collision::doNothing;
	instr[IA][SPEED] = &Collision::onBonus;
	instr[IA][RANGE] = &Collision::onBonus;
	instr[IA][NBBOMB] = &Collision::onBonus;
	instr[IA][NOTHING] = &Collision::goAhead;

	instr[DEFLAGRATION][WALL] = &Collision::doNothing;
	instr[DEFLAGRATION][PLAYER] = &Collision::killAndGo;
	instr[DEFLAGRATION][IA] = &Collision::dieRight;
	instr[DEFLAGRATION][BOX] = &Collision::dieRight;
	instr[DEFLAGRATION][DEFLAGRATION] = &Collision::goAhead;
	instr[DEFLAGRATION][BOMB] = &Collision::goAhead;
	instr[DEFLAGRATION][SPEED] = &Collision::dieRight;
	instr[DEFLAGRATION][RANGE] = &Collision::dieRight;
	instr[DEFLAGRATION][NBBOMB] = &Collision::dieRight;
	instr[DEFLAGRATION][NOTHING] = &Collision::goAhead;
}

Collision::~Collision()
{}

Collision::Collision(Collision const &)
{}

Collision 	&Collision::operator=(Collision const &)
{
	return *this;
}

bool 			Collision::onBonus(entity *ent, entity *tmp)
{
	eType 		bontype;

	bontype = static_cast<Component::Type *>((*tmp)[Component::TYPE])->geteType();
	takeBonus(ent, bontype);
	static_cast<Component::Alive *>((*tmp)[Component::ALIVE])->setRebirth();
	static_cast<Component::IsTaken *>((*tmp)[Component::ISTAKEN])->takeIt();
	std::cout << static_cast<Component::Inventory *>((*ent)[Component::INVENTORY])->getNbBomb() << std::endl;
	return true;
}

bool 			Collision::killAndGo(entity *, entity *tmp)
{
	static_cast<Component::Alive *>((*tmp)[Component::ALIVE])->setDeath();
	return true;
}

bool 			Collision::dieRight(entity *, entity *tmp)
{
	eType 		type;

	type = static_cast<Component::Type *>((*tmp)[Component::TYPE])->geteType();
	if (type == SPEED || type == NBBOMB || type == RANGE)
		static_cast<Component::Alive *>((*tmp)[Component::ALIVE])->setRebirth();
	else	
		static_cast<Component::Alive *>((*tmp)[Component::ALIVE])->setDeath();
	return false;
}

bool 			Collision::dieLeft(entity *ent, entity *)
{
	static_cast<Component::Alive *>((*ent)[Component::ALIVE])->setDeath();
	return false;
}

bool			Collision::goAhead(entity *, entity *)
{
	return true;
}

bool			Collision::doNothing(entity *, entity *)
{
	return false;
}

bool			Collision::isColliding(t_pos x, t_pos y, entity *ent)
{
	t_entlist entlist;
	t_entlistIt it;
	eType		entype;
	eType		tmtype;
	bool 		ret = true;

	if ((static_cast<Component::Type *>((*ent)[Component::TYPE]))->geteType() == PLAYER && !(static_cast<Component::Alive *>((*ent)[Component::ALIVE]))->isAlive())
		return true;
	if (x < g_world->getX() && y < g_world->getY() && (int)x >= 0 && (int)y >= 0)
	{
		(*g_world)(x, y, entlist);
		for (it = entlist.begin(); it != entlist.end(); ++it)
		{
			entype = static_cast<Component::Type *>((*ent)[Component::TYPE])->geteType();
			tmtype = static_cast<Component::Type *>((*(*it))[Component::TYPE])->geteType();
			if ((tmtype == NBBOMB || tmtype == SPEED || tmtype == RANGE) && entype == PLAYER && entlist.size() > 1)
				continue;
			ret &= (this->*(instr[entype][tmtype]))(ent, *it);
		}
		return ret;
	}
	return false;
}
