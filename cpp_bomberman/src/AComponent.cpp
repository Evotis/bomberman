//
// AComponent.cc for src in /home/n0t/dev/cpp/bomberman/cpp_bomberman/src
//
// Made by thomas nieto
// Login   <nieto_t@epitech.net>
//
// Started on  Sat May 10 17:03:46 2014 thomas nieto
// Last update Sat May 10 17:03:46 2014 thomas nieto
//

#include "AComponent.hpp"

// ctors & dtors
Component::AComponent::AComponent() {}

Component::AComponent::~AComponent() {}

// getters
Component::type_t Component::AComponent::getType() const {
  return type_;
}
